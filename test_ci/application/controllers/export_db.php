<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo "<pre>";
//print_r($_SERVER['REQUEST_URI']);
print_r($_REQUEST);
echo "</pre>";

class Export_db extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index

http://..../codeigniter/index.php/export_db

	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
//echo "class Export_db CI_Controller, codeigniter/application/controllers/export_db.php";
//echo "<br>";

		if (empty($_REQUEST['fs_path']))
		{
			//$this->load->view('export_db',$data);
			$this->load->view('export_db');
//$this->load->model('Blog');
//$data['query'] = $this->Blog->get_last_ten_entries();
//$this->load->view('blog', $data);
		}
		else
		{
			$fs_path = $_REQUEST['fs_path'];
			$filename = $_REQUEST['filename'];
			$url_path = $_REQUEST['url_path'];

			$config['hostname'] = 'localhost';
			$config['username'] = 'root';
			$config['password'] = 'master';
			$config['database'] = 'gravura';

			//$config['hostname'] = 'sql306.500mb.net';
			//$config['username'] = 'runet_11119978';
			//$config['password'] = 'w0rdpasss';
			//$config['database'] = 'runet_11119978_gravura';

			$config['dbdriver'] = "mysql";
			$config['dbprefix'] = "";
			$config['pconnect'] = FALSE;
			$config['db_debug'] = TRUE;
			$this->load->model('Export_db_model', '', $config);
//$this->load->model('Export_db_model', '', TRUE);
//$this->Export_db_model->test();
//$data['nodes'] = $this->Export_db_model->test();

//------------------ верхний уровень списка книг 
			$data['books'] = $this->Export_db_model->get_list_book();
//echo "<pre>";
//print_r($data['books']);
//echo "</pre>";
//echo "mlid = ".$data['books'][0]->mlid;
//echo "<br>";

//------------------  2 уровень списка книг (тетради)
			$data['notes'] = $this->Export_db_model->get_list_notes($data['books'][0]->mlid);
//echo "<pre>";
//print_r($data['notes']);
//echo "</pre>";
//echo "mlid = ".$data['notes'][0]->mlid;
//echo "<br>";

//------------------ 3 уровень списка книг (страницы)
			for ($n1=0; $n1<count($data['notes']); $n1++)
			{
				$data['pages'][$n1] = $this->Export_db_model->get_list_pages($data['notes'][$n1]->mlid);
			}
//echo "<pre>";
//print_r($data['pages']);
//echo "</pre>";

//------------------ получить параметры страниц тетради
			for ($n1=0; $n1<count($data['notes']); $n1++)
			{
//echo $n1;
//echo "<br>";
//echo "<ul>";
				for ($n2=0; $n2<count($data['pages'][$n1]); $n2++)
				{
//echo $n2;
					if (!empty($data['pages'][$n1][$n2]->mlid))
					{
//echo ", mlid =".$data['pages'][$n1][$n2]->mlid;
//echo "<br>";

$data['pages'][$n1][$n2]->node = $this->Export_db_model->get_page_info($data['pages'][$n1][$n2]->mlid);
$data['pages'][$n1][$n2]->node['files'] = $this->Export_db_model->get_page_files($data['pages'][$n1][$n2]->node[0]->nid);

//echo "<pre>";
//print_r($data['pages'][$n1][$n2]->node);
//echo "</pre>";	
//echo $data['pages'][$n1][$n2]->node[0]->title;
//echo "<br>";	
					}
				}//---------------------------- end for
//echo "</ul>";
			}//------------------------ end for

//$data['pages'][8][0]->node = $this->Export_db_model->get_page_info($data['pages'][8][0]->mlid);
//$data['pages'][8][0]->node['files'] = $this->Export_db_model->get_page_files($data['pages'][8][0]->node[0]->nid);
//echo "<pre>";
//print_r($data['pages'][8][0]->node);
//echo "</pre>";	

//---------------------------------- Write XML
			$this->write_xml($data);
		}//---------------------------------- end if

	}//----------------------------------- end func
	
	public function write_xml($data)
	{
//echo "public function write_xml(data)";
//echo "<br>";
//echo "<pre>";
//print_r($data['pages'][8][0]);
//echo "</pre>";

	date_default_timezone_set('Asia/Novosibirsk');
	$xml = "";
	$xml .= "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
	$xml .= "<book>\n";
	$xml .= "<title>".$data['books'][0]->link_title."</title>\n";
	$n1=0;
	$n2=0;
	$num_notebooks=0;
	$num_nodes=0;

	// 2 уровень списка книг (тетради)
	//foreach ($notes as $key => $item)
	foreach ($data['notes'] as $key => $item)
	{
		$xml .= "<notebook title=\"".$item->link_title."\">\n";

		// 3 уровень списка книг (страницы)
		//for ($n2=0; $n2<count($pages[$n1]); $n2++)
		for ($n2=0; $n2<count($data['pages'][$n1]); $n2++)
		{
//echo $data['pages'][$n1][$n2]->node[0]->title;
//echo "<br>";	
if($data['pages'][$n1][$n2]->node[0]->status==1)//включать только опубликованные ноды
{
			//параметры страницы тетради
			$xml .=  "<page title=\"".htmlspecialchars($data['pages'][$n1][$n2]->link_title)."\" ";
			$xml .=  "nid=\"".$data['pages'][$n1][$n2]->node[0]->nid."\">";
/*		
			if (!empty($data['pages'][$n1][$n2]->node[0]->title))
			{
				$xml .=  "<title>";
				$xml .=  htmlspecialchars($data['pages'][$n1][$n2]->node[0]->title);
				$xml .=  "</title>\n";
			}
*/

			if (!empty($data['pages'][$n1][$n2]->node[0]->status))
			{
				$xml .=  "<status>";
				$xml .=  $data['pages'][$n1][$n2]->node[0]->status;
				$xml .=  "</status>\n";
			}
			
			if (!empty($data['pages'][$n1][$n2]->node[0]->created))
			{
				$xml .=  "<created ";
				$xml .=  "timestamp=\"".$data['pages'][$n1][$n2]->node[0]->created."\">";
				$xml .=  date('d-M-Y H:i:s', $data['pages'][$n1][$n2]->node[0]->created);
				$xml .=  "</created>\n";
			}

			if (!empty($data['pages'][$n1][$n2]->node[0]->changed))
			{
				$xml .=  "<changed ";
				$xml .=  "timestamp=\"".$data['pages'][$n1][$n2]->node[0]->changed."\">";
				$xml .=  date('d-M-Y H:i:s', $data['pages'][$n1][$n2]->node[0]->changed);
				$xml .=  "</changed>\n";
			}

			if (!empty($data['pages'][$n1][$n2]->node[0]->body_value))
			{
				$body = htmlspecialchars ($data['pages'][$n1][$n2]->node[0]->body_value);
//-------------------------------- фильтровать текст страниц
/*
				if (isset($_REQUEST['filter_body']))
				{
					if ($_REQUEST['filter_body']=='on')
					{
//echo "<pre>";
//echo $body;
//echo "</pre>";
					}
				}
*/
//-------------------------------- 
				$xml .=  "<body_value>";
				$xml .=  $body;
				$xml .=  "</body_value>\n";
			}

			if (!empty($data['pages'][$n1][$n2]->node[0]->field_notebook_tid))
			{
				$xml .=  "<termin tid=\"";
				$xml .=  $data['pages'][$n1][$n2]->node[0]->field_notebook_tid."\" ";
				$xml .=  "name=\"".htmlspecialchars($data['pages'][$n1][$n2]->node[0]->name)."\">";
				$xml .=  "</termin>";
			}

			if (!empty($data['pages'][$n1][$n2]->node[0]->page_title))
			{
				$xml .=  "<page_title>";
				$xml .=  htmlspecialchars($data['pages'][$n1][$n2]->node[0]->page_title);
				$xml .=  "</page_title>";
			}

			if (!empty($data['pages'][$n1][$n2]->node[0]->alias))
			{
				$xml .=  "<alias>";
				$xml .=  htmlspecialchars($data['pages'][$n1][$n2]->node[0]->alias);
				$xml .=  "</alias>";
			}

			if (!empty($data['pages'][$n1][$n2]->node[0]->field_large_img_value))
			{
				$xml .=  "<field_large_img_value>";
				$xml .=  $data['pages'][$n1][$n2]->node[0]->field_large_img_value;
				$xml .=  "</field_large_img_value>\n";
			}

			if (!empty($data['pages'][$n1][$n2]->node[0]->field_medium_img_value))
			{
				$xml .=  "<field_medium_img_value>";
				$xml .=  $data['pages'][$n1][$n2]->node[0]->field_medium_img_value;
				$xml .=  "</field_medium_img_value>\n";
			}

			if (!empty($data['pages'][$n1][$n2]->node[0]->field_original_img_value))
			{
				$xml .=  "<field_original_img_value>";
				$xml .=  $data['pages'][$n1][$n2]->node[0]->field_original_img_value;
				$xml .=  "</field_original_img_value>\n";
			}

			if (!empty($data['pages'][$n1][$n2]->node[0]->field_preview_gallery_img_value))
			{
				$xml .=  "<field_preview_gallery_img_value>";
				$xml .=  $data['pages'][$n1][$n2]->node[0]->field_preview_gallery_img_value;
				$xml .=  "</field_preview_gallery_img_value>\n";
			}

			if (!empty($data['pages'][$n1][$n2]->node[0]->field_small_img_value))
			{
				$xml .=  "<small_img>";
				$xml .=  $data['pages'][$n1][$n2]->node[0]->field_small_img_value;
				$xml .=  "</small_img>\n";
			}

			if (count($data['pages'][$n1][$n2]->node['files'])>0)
			{
				$xml .=  "<files>";
				for ($n3=0; $n3<count($data['pages'][$n1][$n2]->node['files']); $n3++)
				{
					$xml .=  "<file>";
					$xml .=  "	<filename>";
					$xml .=  trim($data['pages'][$n1][$n2]->node['files'][$n3]->filename);
					$xml .=  "	</filename>\n";

					$xml .=  "	<uri>";
					$xml .=  $data['pages'][$n1][$n2]->node['files'][$n3]->uri;
					$xml .=  "	</uri>\n";

					$xml .=  "	<filemime>";
					$xml .=  $data['pages'][$n1][$n2]->node['files'][$n3]->filemime;
					$xml .=  "	</filemime>\n";

					$xml .=  "	<filesize>";
					$xml .=  $data['pages'][$n1][$n2]->node['files'][$n3]->filesize;
					$xml .=  "	</filesize>\n";

					$xml .=  "	<time ";
					$xml .=  "timestamp=\"".$data['pages'][$n1][$n2]->node['files'][$n3]->timestamp."\">";
					$xml .=  date('d-M-Y H:i:s', $data['pages'][$n1][$n2]->node['files'][$n3]->timestamp);
					$xml .=  "	</time>\n";

					$xml .=  "</file>\n";
				}
				$xml .=  "</files>\n";
			}
			$xml .= "</page>\n";
			$num_nodes++;
}//--------------------- end if (node status)

		}//---------------------------- end for
		$xml .= "</notebook>\n";
		$num_notebooks++;
		$n1++;
	}//--------------------------- end foreach
	$xml .= "</book>\n";

//-------------------------------------- write XML
		if (isset($_REQUEST['save']))
		{
			if ($_REQUEST['save']!='on')
			{
				$xml="";
			}
		}
		else
			$xml="";
//echo getcwd();
		$message="";
		$message .= "Выполнен экспорт "
."тетрадей (".$num_notebooks."), "
."страниц (".$num_nodes.")";
		$message .= "<br>";

		if (!empty($xml))
		{
//echo $xml;
			//директория с файлами для экспорта
			if (!empty($_REQUEST['fs_path']))
			{
				$fs_path = $_REQUEST['fs_path'];
				$filename = $_REQUEST['filename'];
				$url_path = $_REQUEST['url_path'];
			}
			else
			{
				$fs_path = getcwd();
				$filename = 'export_gravura.xml';
				$url_path = '';
			}

			$num_bytes = file_put_contents ($fs_path."/".$filename, $xml);
			if ($num_bytes > 0)
			{
$message .= "<span class='ok'>Write </span>".$num_bytes." bytes  in ".$filename;
$message .= "<br>";
//$log .= "base_url = ".$base_url;
//$log .= "<br>";
		
$message .= "<a href='".$url_path."/".$filename."'>".$filename."</a>";
//echo "<pre>";
//readfile ($output_filename);
//echo "</pre>";
			}
			else
			{
$message .= getcwd();
$message .= "<br>";
$message .= "<span class='error'>Write error in </span>".$filename;
$message .= "<br>";
			}

		}

$message .= "<br>";
echo $message;

//--------------------------------------

	}//---------------------- end func

}//--------------- end class
