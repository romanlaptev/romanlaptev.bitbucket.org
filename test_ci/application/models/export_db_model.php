<?php 
//echo "application/<font color='blue'><b>models/export_db.php</b></font>";
//echo "<br>";
error_reporting(E_ALL);

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Export_db_model extends CI_Model
{
    var $title   = '12345';
    var $content = '';
    var $date    = '';

    //function Export_db_model()
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
//echo "<ul>";
//echo "class <b>Export_db_model extends Model</b>, function <b>Export_db_model()</b>";
//echo "</ul>";
    }

    function test()
    {
echo "<ul>";
echo "function <b>test()</b>";
echo "</ul>";

//Эта функция позволяет задать оператор WHERE с помощью одного из четырех способов:
//Примечание: Для получения более безопасных запросов, все значения переданные в эту функцию автоматически заключаются в //одинарные кавычки.
// Генерирует: WHERE type = 'book' 
    $this->db->where('type', 'book');

//Позволяет задать имена выбираемых полей в вашем SELECT запросе:
//SELECT nid, type, title FROM node
	$this->db->select('nid, type, title');
	$query = $this->db->get('node');
        return $query->result();
    }


/*    
$this->db->join();
Позволяет задать JOIN часть вашего запроса:
$this->db->select('*');
$this->db->from('blogs');
$this->db->join('comments', 'comments.id = blogs.id');
$query = $this->db->get();

// Генерирует:
// SELECT * FROM blogs
// JOIN comments ON comments.id = blogs.id

Возможно несколько вызовов функции, если необходимо сделать несколько объединений в одном запросе.

Если же вам нужно что-то отличное от обычного JOIN, вы может сделать это посредством третьего параметра функции. Допустимые значения: left, right, outer, inner, left outer и right outer.
$this->db->join('comments', 'comments.id = blogs.id', 'left');

// Генерирует: LEFT JOIN comments ON comments.id = blogs.id
*/

    function get_last_ten_nodes()
    {
        $query = $this->db->get('node', 10);
        return $query->result();
    }

//-------------------------------------
//-- верхний уровень списка книг 
//-------------------------------------
    function get_list_book()
    {
/*
SELECT * FROM menu_links WHERE menu_links.module='book' AND menu_links.depth=1
UNION
SELECT * FROM menu_links WHERE menu_links.plid IN -- ВЛОЖЕННЫЙ ЗАПРОС
(SELECT menu_links.mlid FROM menu_links WHERE menu_links.module='book' AND menu_links.depth=1);
--------------
*/
/*
-- 2 уровень списка книг (содержание)
SELECT *
FROM menu_links
WHERE 
menu_links.plid=349;
	$this->db->where('menu_links.plid', '349');
	$this->db->select('mlid, plid, link_title');
	$query = $this->db->get('menu_links');
*/
//SELECT `mlid`, `plid`, `link_title` FROM (`menu_links`) 
//WHERE `menu_links`.`module` = 'book' AND `menu_links`.`depth` = '1'
	$this->db->where('menu_links.module', 'book');
	$this->db->where('menu_links.depth', '1');
	$this->db->select('mlid, plid, link_title');
	$query = $this->db->get('menu_links');

        return $query->result();
    }

//-------------------------------------
//-- 2 уровень списка книг (тетради)
//-------------------------------------
    function get_list_notes($book_id)
    {
//echo "book_id = ".$book_id;
//echo "<br>";
/*
-- 2 уровень списка книг (содержание)
SELECT *
FROM menu_links
WHERE 
menu_links.plid=349;
*/
	$this->db->where("menu_links.plid","$book_id");
	$this->db->order_by("weight", "asc"); 
	$this->db->select("mlid, plid, link_title");
	$query = $this->db->get("menu_links");

        return $query->result();
    }

//-------------------------------------
//-- 3 уровень списка книг (страницы)
//-------------------------------------
    function get_list_pages($note_id)
    {
//echo "note_id = ".$note_id;
//echo "<br>";
/*

-- 3 уровень списка книг (страницы)
SELECT *
FROM menu_links
WHERE 
menu_links.plid=370;

*/
	$this->db->where("menu_links.plid","$note_id");
	$this->db->order_by("weight", "asc"); 
	$this->db->select("mlid, plid, link_title");
	$query = $this->db->get("menu_links");


        return $query->result();
    }
//-------------------------------------
// получить параметры страницы тетради
//-------------------------------------
    function get_page_info($note_id)
    {
//echo "note_id = ".$note_id;
//echo "<br>";
/*
	$this->db->where("node.nid","book.nid",FALSE);
	$this->db->select("node.nid, node.title, node.status, node.created, node.changed, field_data_body.body_value");
	$this->db->join("book", "book.mlid=".$note_id);
	$this->db->join("field_data_body", "field_data_body.entity_id=node.nid");
	$query = $this->db->get("node");
*/
	// получить nid, соответствующий странице
	$sql = "SELECT nid FROM book WHERE book.mlid=$note_id;";
	$query = $this->db->query($sql);
	$nid_arr = $query->result();
//echo "<pre>";
//print_r($nid_arr);
//echo "</pre>";
	$nid = $nid_arr[0]->nid;
//echo "nid = ".$nid;
//echo "<br>";

	$sql = "
SELECT
node.nid, 
-- node.type, 
node.title, 
node.status, 
node.created, 
node.changed, 
field_data_body.body_value, 
-- =============================== поле расположение картинк по размерам
field_data_field_large_img.field_large_img_value, 
field_data_field_medium_img.field_medium_img_value, 
field_data_field_original_img.field_original_img_value, 
field_data_field_preview_gallery_img.field_preview_gallery_img_value, 
field_data_field_small_img.field_small_img_value, 
-- ====================================================== термин словаря
field_data_field_notebook.field_notebook_tid, 
taxonomy_term_data.name, 
-- ============================================== заголовок HTML-страницы
-- page_title.type
page_title.page_title, 
-- url страницы
-- url_alias.pid
url_alias.alias 
-- ======================================================================
FROM node
LEFT JOIN book ON book.mlid=$note_id
LEFT JOIN field_data_body ON field_data_body.entity_id=node.nid -- основная часть
LEFT JOIN field_data_field_large_img ON field_data_field_large_img.entity_id=node.nid -- поле картинки large
LEFT JOIN field_data_field_medium_img ON field_data_field_medium_img.entity_id=node.nid -- поле картинки medium
LEFT JOIN field_data_field_original_img ON field_data_field_original_img.entity_id=node.nid -- поле картинки original
LEFT JOIN field_data_field_preview_gallery_img ON field_data_field_preview_gallery_img.entity_id=node.nid -- поле картинки preview
LEFT JOIN field_data_field_small_img ON field_data_field_small_img.entity_id=node.nid -- поле картинки small
LEFT JOIN field_data_field_notebook ON field_data_field_notebook.entity_id=node.nid -- tid термина словаря
LEFT JOIN taxonomy_term_data ON taxonomy_term_data.tid=field_data_field_notebook.field_notebook_tid
LEFT JOIN page_title ON page_title.id=node.nid -- заголовок страницы
LEFT JOIN url_alias ON url_alias.source='node/$nid' -- url страницы
WHERE 
node.nid=book.nid 
-- AND node.status=1
;";
	$query = $this->db->query($sql);
        return $query->result();
    }//---------------------------- end func

//-----------------------------------------
// получить параметры прикрепленных файлов
//-----------------------------------------
    function get_page_files($nid)
    {
//echo "nid = ".$nid;
//echo "<br>";
	$sql = "
-- получить параметры прикрепленных файлов
SELECT
file_managed.filename, 
file_managed.uri, 
file_managed.filemime, 
file_managed.filesize, 
file_managed.timestamp
FROM file_managed
LEFT JOIN node ON node.nid='$nid' -- nid страницы
LEFT JOIN file_usage ON file_usage.id=node.nid -- fid прикрепленных файлы  
WHERE file_managed.fid=file_usage.fid;
";
	$query = $this->db->query($sql);
        return $query->result();
    }//---------------------------- end func

}//------------------------------ end class
?>
