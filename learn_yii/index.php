<?php
error_reporting(E_ALL|E_STRICT);
//ini_set('display_errors', 1);
ini_set("display_errors", true);

require_once "inc/config.php";
//=============================== check PHP version
$required_ver = '5.1.0';
$res = version_compare( PHP_VERSION,  $required_ver );
//echo "version_compare: ". $res;
//echo "<br/>\n";
switch( $res ){

	case -1:
		$msg = "Your PHP version ".PHP_VERSION." < ".$required_ver;
		echo $msg;
		echo "<br\n>";

		$msg = "error, could not run application...";
		echo $msg;
		echo "<br\n>";

		$msg = "<a href='".$yii_path."/requirements/'>look at the minimal requirements here...</a>";
		echo $msg;
		echo "<br\n>";

		exit;
	break;
/*
	case 0:
echo "Your PHP version === ".$required_ver ;
echo "<br/>\n";
	break;

	case 1:
echo "Your PHP version > ".$required_ver ;
echo "<br/>\n";
	break;
*/
}//end switch

require_once "inc/utils.php";
//echo _logWrap($_SERVER);
//exit;
require_once "app.php";
?>
