<?php

function _logWrap( $msg, $level = "info"){

	// check API type
	$sapi_type = php_sapi_name();
//echo "php_sapi_name: ". $sapi_type;
//echo "<br/>\n";
//echo "type: ". gettype( $msg);
//echo "<br/>\n";

//-------------
	//$runType = "";
	//if ( $sapi_type == 'apache2handler' || 
	//		$sapi_type == 'cli-server'
		//) {
		$runType = "web";
	//}

	if ( $sapi_type == "cli" ) { $runType = "console"; }
	if ( $sapi_type == "cgi" ) { $runType = "console"; }

//-------------
	if( gettype( $msg) === "array" || 
		gettype( $msg) === "object"
	){
			if ( $runType == "web" ) {
				$out = "<pre>".print_r($msg,1)."</pre>";
				return $out;
			} else {
				$out = print_r($msg,1)."\n";
				return $out;
			}
	}

	if( gettype( $msg) !== "string"){
		return false;
	}

//-------------
	switch ($level) {
		case "info":
			if ( $runType == "web" ) {
				return "<div class='alert alert-info'>".$msg."</div>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
		
		case "warning":
			if ( $runType == "web" ) {
				return "<div class='alert alert-warning'>".$msg. "</div>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
		
		case "danger":
		case "error":
			if ( $runType == "web" ) {
				return "<div class='alert alert-danger'>".$msg. "</div>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
		
		case "success":
			if ( $runType == "web" ) {
				return "<div class='alert alert-success'>".$msg. "</div>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
		
		default:
			if ( $runType == "web" ) {
				return $msg. "<br/>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
	}//end switch

}//end _logWrap()

function runDBquery( $db, $sql_query ){

	$result  = $db->query( $sql_query );
	if( !$result ){
		$msg =  "-- error, query: ".$sql_query;
		echo _logWrap( $msg, "error" );
		echo "error info: ". _logWrap( $connection->errorInfo() );
$arr = $connection->errorInfo();
$desc = $arr[2];
		return array(
"type" => "error",
"description" => $desc
		);
	}

	//$msg =  "-- run query: ".$sql_query;
	//echo _logWrap( $msg, "success" );

	//$rows  = $result->fetchAll( PDO::FETCH_NUM );
	$rows  = $result->fetchAll( PDO::FETCH_ASSOC );
//echo count( $rows );
	if( count( $rows ) > 0 ){
		return array(
"type" => "success",
"data" => $rows
		);
	}

	return array("type" => "success");
}//end runDBquery()

?>
