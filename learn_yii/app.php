<?php
error_reporting(E_ALL|E_STRICT);
//ini_set('display_errors', 1);
ini_set("display_errors", true);

//ini_set("session.gc_maxlifetime", 60 );//86400 sec
//$lifetime=65;
//session_set_cookie_params($lifetime);

runApp( $yii_path );

//---------------------------
function runApp( $yii_path ){
	// change the following paths if necessary
	//$yii=dirname(__FILE__).'/../../var/www/php/yii/framework/yii.php';
	//$yii='../../../../php/frameworks/yii/framework/yii.php';
	$yii=$yii_path.'framework/yii.php';

	$config=dirname(__FILE__).'/protected/config/main.php';
	$res = checkDb( $config );
	if( !$res ){
		$msg = "error, wrong database";
		$msg .= ", could not run application...";
		echo _logWrap( $msg, "error" );
		return;
	}


	// remove the following lines when in production mode
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	// specify how many levels of call stack should be shown in each log message
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

	require_once($yii);
	Yii::createWebApplication($config)->run();

//echo "Yii::app:<pre>";
//print_r( Yii::app() );
//echo "</pre>";

}//end runApp()


//---------------------------
function checkDb( $config=null ){

	$res = false;
	
	if (is_string($config) ){
		$conf = require($config);
	}
//echo _logWrap( $conf["components"]["db"] );

	//---------------------------
	$moduleName = "sqlite3";
	$loadedExt = get_loaded_extensions();
	if ( !in_array( $moduleName, $loadedExt ) ) {
		$msg = "-- error, $moduleName module  is not in the list of loaded extensions";
		$msg .= ", could not run application...";
		echo _logWrap($msg, "error");
		echo "loaded_extensions:". _logWrap($loadedExt);
		return $res;
	}

	$pos = strpos( $conf["components"]["db"]["connectionString"], "sqlite");
// 	echo "pos:". $pos.", type:". gettype($pos);
// 	echo "<br>\n";
// 	
	//check  file SQLITE database
	if( $pos !== false ){
	
		//[connectionString] => sqlite:/mnt/serv_d1/www/projects/Kontur_test/test_app/protected/config/../data/db1.sqlite
		$split_arr = explode( ":", $conf["components"]["db"]["connectionString"]);
		$filePath = $split_arr[1];
 		//if ( file_exists( $filePath ) )	{
			return checkDbTables( $conf["components"]["db"] );
 		//} else {
 			//$msg = "error, not found database: ".$filePath;
 			//echo _logWrap( $msg, "error");
 			//return false;
 		//}
		
	}

	return $res;
}//end checkDb()

function checkDbTables( $config ){

	try {
		$db = new PDO( $config["connectionString"] );
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$sql_query = "CREATE TABLE IF NOT EXISTS `users` (
		  `user_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		  `login` VARCHAR(64) NOT NULL,
		  `password` VARCHAR(64) NOT NULL
		); ";
		
		//$db->query($sql_query);
		//$msg = "table $tableName created succesfully.";
		//echo _logWrap( $msg, "success");
		//$msg = "SQL: " . $sql_query;
		//echo _logWrap( $msg, "info");
		$result = runDBquery( $db, $sql_query);
		
		//$sql_query = "INSERT INTO `users` (
//`user_id`, `login`, `password`) 
//VALUES
//(1, 'admin', '1b3231655cebb7a1f783eddf27d254ca');";
		//$result = runDBquery( $db, $sql_query);
		
		$sql_query = "CREATE TABLE IF NOT EXISTS `courses` (
`course_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
`title` VARCHAR(255) NOT NULL DEFAULT 'курс',
`description` TEXT
);";
		$result = runDBquery( $db, $sql_query);

		$sql_query = "CREATE TABLE IF NOT EXISTS `lessons` (
`lesson_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
`course_id` INTEGER(6) DEFAULT NULL,
`url` VARCHAR(255) NOT NULL DEFAULT 'https://www.youtube.com/embed/code_video',
`title` VARCHAR(255) NOT NULL DEFAULT 'урок',
`description` TEXT
);";
		$result = runDBquery( $db, $sql_query);
//echo _logWrap( $result );
		return true;
		
	} catch (Exception $e) {
		echo _logWrap( $e, "error");
		return false;
	}	

	return false;
}//end checkDbTables()

?>
