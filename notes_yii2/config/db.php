<?php
/*
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=db1',
    'username' => 'root',
    'password' => 'master',
    'charset' => 'utf8',
];
*/
return [
    'class' => 'yii\db\Connection',
    //'dsn' => 'sqlite:'.dirname(__FILE__).'/../../learn_yii/data/learn.sqlite',
	'dsn' => 'sqlite:'.dirname(__FILE__).'/../data/notes.sqlite',
];

