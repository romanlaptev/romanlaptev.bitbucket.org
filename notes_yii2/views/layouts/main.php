<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Notes</title>
    <?php $this->head() ?>

	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="apple-touch-icon" href="favicon.png">
	<link rel="stylesheet" href="css/bootstrap441.min.css">

</head>
<body>

<?php $this->beginBody() ?>


<div class="container">

	<header>
		<div class="page-header">
			<h1>Notes</h1>
		</div>
	</header>

        <div>
<?//= Html::encode($this->title) ?>

<?= Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>
        </div>

	<div class="panel" id="mainmenu">
		<div class="panel-body">
<?php
echo Menu::widget([
		'options' => ['class' => 'list-inline'],
		'itemOptions'=> ['class' => 'list-inline-item'],
        'items' => [
			['label' => 'Home', 'url' => ['/site/index'] ],
			['label' => 'About', 'url' => ['/site/about'] ],
['label' => 'add new note', 'url' => ['#'] ],
["label" => "Export", "url" => ["#"] ],
["label" => "Import", "url" => ["#"] ],
["label" => "remove all notes", "url" => ["#"] ],
["label" => "clear table", "url" => ["#"] ],

//				Yii::$app->user->isGuest ?
//				['label' => 'Login', 'url' => ['/site/login']] :
//				['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
//					'url' => ['/site/logout'],
//					'linkOptions' => ['data-method' => 'post']],
        ],
]);
?>
        </div>
    </div>

	<div class="panel panel-default">
		<div class="panel-body">
<?= $content ?>
        </div>
    </div>

	<footer class="footer">
			<p class="pull-left"></p>
			<p class="pull-right"><?= Yii::powered()." ".Yii::getVersion(); ?></p>
	</footer>

</div><!-- end container -->



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
