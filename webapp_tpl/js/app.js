var func = sharedFunc();
console.log("func:", func);

window.onload = function(){

func.logAlert( navigator.userAgent, "info");
	
console.log( webApp );	
/*
	webApp.vars["isRunApp"] = false;

	for( var n = 0; n < webApp.vars["tests"].length; n++ ){
		var _test = webApp.vars["tests"][n];
//console.log(_test);
		if( _test["result"]){
			webApp.vars["isRunApp"] = true;
		} else {
			webApp.vars["isRunApp"] = false;
			break;
		}
	}//next

	if( webApp.vars["support"]["jsonSupport"]){
		webApp.vars["isRunApp"] = true;
	}

	// if( webApp.vars["support"]["swSupport"] && 
			// webApp.vars["support"]["cacheSupport"] && 
				// webApp.vars["promiseSupport"] ){
		// registerServiceWorker();
	// }
	
	if( !webApp.vars["isRunApp"] ){
		webApp.vars["logMsg"] = "error, webApplication is not running in this browser....";
func.logAlert(webApp.vars["logMsg"], "error");
console.log( webApp.vars["logMsg"] );
		return false;
	}
	
*/	
	//Start webApp
	if( typeof webApp === "object"){
		var webApp = _app();
		
		var timeStart = new Date();
		webApp.init(function(){
console.log("end webApp initialize....");
			_runApp({
					"tplName": "materialize_tpl"
				});
				
					var timeEnd = new Date();
					var ms = timeEnd.getTime() - timeStart.getTime();
					var msg = "Generate content block, nid: " + this.nid +", runtime:" + ms / 1000 + " sec";
_log("<p>"+msg+"</p>");			
console.log(msg);
console.log("-- end build page --");
					webApp.app.vars["runtime"].push({
						"source" : msg,
						"ms" : ms,
						"sec" : ms / 1000
					});
				
		});
	}
console.log(webApp);
	
}//end load


var _app = function ( opt ){
//console.log(arguments);
	var _vars = {
		"init_url" : "?q=load-xml",
		"requestUrl" : "data/export.xml",
		//"requestUrl" : "/mnt/d2/temp/export_mydb_allnodes.xml",
		
/*
		"dataSourceType" : "use-rpc-requests", //use-rpc-requests, load-local-file, use-local-storage

		"localRequestPath" : "data/export.xml",
		//"localRequestPath" : "/mnt/d2/temp/export_mydb_allnodes.xml",

		"serverUrl" : "/projects/romanlaptev.heroku/projects/db_admin/",
//		"serverUrl" : "https://romanlaptev-cors.herokuapp.com/\
//https://romanlaptev.herokuapp.com/projects/db_admin/",
		//"serverUrl" : "https://romanlaptev.herokuapp.com/projects/db_admin/",

*/		
		"templates" : {
			"tpl-node" : _getTpl("tpl-node"),
			"tpl-page-list" : _getTpl("tpl-page-list"),
			"tpl-page-list-item" : _getTpl("tpl-page-list-item"),
			"tpl-booklist" : "<h3>Book list</h3><ul>{{list}}</ul>"
		},
		"appContainer" : func.getById("App"),
		"contentList" : func.getById("content-list"),
		//"controlPanel" : func.getById("control-btn"),
		"log" :  func.getById("log"),
		"btnToggle" : func.getById("btn-toggle-log"),
		"breadcrumbs": {},
		
		"csv_delimiterByFields" : ",",
		"csv_delimiterByLines" : "\r\n",
					
		"waitWindow" : func.getById("win1"),
		"waitWindowLoad" : func.getById("win-load"),
		
		"loadProgress" : func.getById("load-progress")	,
		"loadProgressBar" : func.getById("load-progress-bar"),
		
		"numTotalLoad" : func.getById("num-total-load"),
		"percentComplete" : func.getById("percent-complete"),

"use_localcache" : true,//false,//
		
		//"totalBytes" : func.getById("total"),
		//"totalMBytes" : func.getById("total-mb"),
		//"loaded" : func.getById("loaded"),
		//"loadInfo" : func.getById("load-info")
		
	};

/*
	"vars" : {
		"app_title" : "video collection",
		"logMsg" : "",
		//"messages" : {
			////"storeNotFound" : "<p class='alert alert-danger'>Object store not exists in DB!!!</p>"
			//"nodeNotFound" : "<p class='alert alert-danger'>node not found!!!</p>",
			//"templateNotFound" : "<p class='alert alert-danger'>Not find template, id: <b>{{templateID}}</b></p>"
		//},
		//"templates_url" : "tpl/templates.xml",
		//"GET" : {},
		"DB" : {
			"dataUrl" : "db/export_video.xml",
			"dbType" : "xml",
			//"data_url" :"db/art_correct.json",
			//"db_type" : "json",
			"dbName": "video",
			"tagNameFilms": "video",
			"numRecordsPerPage":10,
			"dateFormat": "dd-mm-yyyy hh:mm:ss",
			"sortOrder": "asc",
			"sortByKey": "title", //"published", 
			"queryRes": []
		},

			{
				"name" : "block-style",
				"title" : "стиль", //"техника",//"жанр",
				"templateID" : "tpl-info_termins_style-block",//location and style for block
				"contentTpl" : "tpl-menu",
				//"contentListTpl" : "tpl-menu_list",
				"contentListTpl" : "tpl-taxonomy-menu_list",
				"content" : function( args ){//function for getting content data
					webApp.db.getBlockContent({
						"vocName" : "info",
						"termName" : "стиль",
						"contentType" : "getTerminByName",
						"callback" : function(res){
//console.log(res);							
							if( typeof args["callback"] === "function"){
								args["callback"]( res );
							}
						}//end callback
					});
				},//end callback()
				"visibility" : "frontPage"
			},//end block

		"blocks": [
			{
				"name" : "block-1",
				"title" : "Title", 
				"templateID" : "tpl-block-1",
				"content" : "<u>static text in block-1</u>"//,
				//"visibility" : "frontPage"
			},//end block
		
			{
				"locationID" : "block-playlist",
				"title" : "Playlist", 
				"templateID" : "tpl-block-playlist",
				"content" : "",
				"visibility" : true,//"frontPage"
				"buildBlock" : function(){
//console.log(this);
					for(var n = 0; n < webApp.modPlayer.vars["playlist"]["tracks"].length; n++){
						webApp.modPlayer.vars["playlist"]["tracks"][n]["number"] = n;
					}//next
					
					var html = _draw_wrapData({
						"data": webApp.modPlayer.vars["playlist"]["tracks"],
						"templateID": "tpl-playlist",
						"templateListItemID": "tpl-playlist-item"
					});
//console.log( html);
//var html = "<h1>...test</h1>";
					if( html && html.length > 0){
						this.content = html;
						_draw_buildBlock( this );
					}
					
					if( webApp.modPlayer.vars["playlist"]["tracks"].length > 0 ){
						_draw_setActiveTrack(0);
					}

				}
			},//end block
			
			{
				"locationID" : "block-links",
				"title" : "footer links", 
				"templateID" : "tpl-block-links",
				"content" : "",
				"visibility":true,
				"buildBlock" : function(){
					_draw_buildBlock( this );
				}
			}, //end block
			
			
			{
				"locationID" : "block-taglist",
				"title" : "block-taglist!!!!!!!",
				"templateID" : "tpl-block-taglist",
				"content" : "...",
				"visibility":true,
				"buildBlock" : function(){
					
					var html = _draw_wrapData({
						"data": webApp.vars["DB"]["tags"],
						"templateID": "tpl-taglist",
						"templateListItemID": "tpl-taglist-item"
					});
//console.log( html);
					if( html && html.length > 0){
						this.content = html;
						_draw_buildBlock( this );
					}
				}
			}, //end block

			{
				"locationID" : "block-search",
				"title" : "block-search",
				"templateID" : "tpl-block-search",
				"content" : "",
				"visibility":true,
				"refresh" : false,
				"buildBlock" : function(){
					_draw_buildBlock( this );
				}
			} //end block

		],
		
		"templates_url" : "tpl/templates.xml",
		"templates" : {
			"localVideoBtn" : "create link",
			"embedVideoBtn" : "open video in new tab",
			"localVideoBtnUpdated" : "<a href='{{url}}' class='btn btn-primary' target='_blank'>open local video file in new tab</a>"
		},
		"init_url" : "#?q=list_nodes&num_page=1"
	},//end vars	
*/	
	var _init = function(){
console.log("init webapp!");
/*
		_defineEvents();
		
		var parseUrl = window.location.search; 
		if( parseUrl.length > 0 ){
			_vars["GET"] = parseGetParams(); 
			_urlManager();
		} else {
			if( _vars["init_url"] ){
					//parseUrl = _vars["init_url"].substring(2);
					parseUrl = _vars["init_url"];
console.log(parseUrl);					
			}
			_vars["GET"] = parseGetParams( parseUrl ); 
			_urlManager();
		}
*/
		this["vars"]["log"] = func.getById("log");
		this["vars"]["btnToggle"] = func.getById("btn-toggle-log");
		this["vars"]["loadProgressBar"] = func.getById("load-progress-bar");
		//this["vars"]["parseProgressBar": func.getById("parse-progress-bar");
		this["vars"]["numTotalLoad"] = func.getById("num-total-load");
		this["vars"]["waitWindow"] = func.getById("win1");
/*		
		webApp.db.init();
		//webApp.iDBmodule.init();
//console.log(iDBmodule, typeof iDBmodule);			
		webApp.draw.init();
		webApp.app.init();
*/
		
		_loadTemplates(function(){
//console.log("Load templates end...", webApp.vars["templates"] );		
			_runApp();
		});		
	};//end _init()
	
	
	
	function _runApp(){

		defineEvents();

		//start block
		if( webApp["vars"]["waitWindow"] ){
			webApp["vars"]["waitWindow"].style.display="block";
			//$("#load-progress").hide();
		}


		_loadData(function(res){
	//console.log(arguments);
	//console.log(window.location);	

			//clear block
	//setTimeout(function(){
			if( webApp["vars"]["waitWindow"] ){
				webApp["vars"]["waitWindow"].style.display="none";
			}		
	//}, 1000*3);

	//if( webApp.vars["loadDataRes"] ){
	if( webApp.vars["DB"]["nodes"] && webApp.vars["DB"]["nodes"].length > 0){
			var parse_url = window.location.search; 
			if( parse_url.length > 0 ){
				webApp.vars["GET"] = func.parseGetParams(); 
				_urlManager();
			} else {
				if( webApp.vars["init_url"] ){
					//parse_url = webApp.vars["init_url"].substring(2);
					parse_url = webApp.vars["init_url"];
		//console.log(parse_url);
				}
				webApp.vars["GET"] = func.parseGetParams( parse_url ); 
				_urlManager();
			}
	}

			if( typeof postFunc === "function"){
				postFunc();
			}
		});

		
	}//end _runApp()	
	
	
	
	function _getTpl( id ){
		var tpl = func.getById(id);
		return tpl.innerHTML;
	}//end _getTpl()
	
	
	function _defineEvents(){
//console.log("_defineEvents()");

		if( !_vars.appContainer ){
_vars["logMsg"] = "error, 'appContainer' undefined, _defineEvents()";
_alert(_vars["logMsg"], "error");
			return false;
		}
			
		_vars.appContainer.onclick = function(event){
			event = event || window.event;
			var target = event.target || event.srcElement;
//console.log( event );
// //console.log( this );//page-container
//console.log( target.tagName );
// //console.log( event.eventPhase );
// //console.log( "preventDefault: " + event.preventDefault );
			// //event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);
			// //event.preventDefault ? event.preventDefault() : (event.returnValue = false);				
			
			if( target.tagName === "A"){
				//if ( target.href.indexOf("#") !== -1){
					if (event.preventDefault) { 
						event.preventDefault();
					} else {
						event.returnValue = false;				
					}
					_vars["GET"] = parseGetParams( target.href ); 
					_urlManager();
				//}
			}
			
					if( target.tagName === "INPUT"){
						if ( target.name === "data_source_type"){
		//console.log( event );
							_vars["dataSourceType"] = target.value; 
						}
					}
			
		}//end event
		
		
/*
//----------------------------------- btn SERVICE PANEL toggle
	func.addEvent( func.getById("btn-service-panel"), "click", 
		function(event){
//console.log( event.type );
			event = event || window.event;
			var target = event.target || event.srcElement;
			if (event.preventDefault) {
				event.preventDefault();
			} else {
				event.returnValue = false;
			}
			updateFormSystem();
		}
	);//end event
*/		

/*
//----------------------------------- ADD NEW NODE (NOTE)
		document.forms["form_node"].onsubmit = function(event){

			event = event || window.event;
			var target = event.target || event.srcElement;
			
			if (event.preventDefault) { 
				event.preventDefault();
			} else {
				event.returnValue = false;
			}
			
//console.log("Submit form", event, this);
			var _form = document.forms["form_node"];
//console.log(form);
//console.log(form.elements, form.elements.length);
//console.log(form.elements["number"]);
//form.action = "?q=save-node";
//console.log(form.action);
			var isValid = checkForm({
					"form" : _form
			});
			
			if( !isValid ){
_vars["logMsg"] = "form validation error";
func.logAlert(_vars["logMsg"], "error");
				return false;
			}
			
			_vars["GET"] = func.parseGetParams( "?q=save-node" ); 
			_urlManager();
			
			_vars["GET"] = func.parseGetParams( "?q=close-form-node" ); 
			_urlManager();
				
			return false;
		};//end event

*/

/*
//--------------- CHANGED SYSTEM variables and reload app
		document.forms["form_system"].onsubmit = function(event){
			event = event || window.event;
			var target = event.target || event.srcElement;
			
			if (event.preventDefault) { 
				event.preventDefault();
			} else {
				event.returnValue = false;
			}
//console.log("Submit form", event, this);
			var form = document.forms["form_system"];
//console.log(form);
			for( var n = 0; n < form.elements.length; n++){
				var _element = form.elements[n];
        if(_element.name === "data_source_type" && _element.checked ){
//console.log( _element.value, _element.checked );
//elmnt.checked = false;
					_vars["dataSourceType"] = _element.value;
					
					if(_element.value === "use-rpc-requests" ){
						_vars["serverUrl"] = form.elements["server_url"].value;
					}
					if(_element.value === "load-local-file" ){
						_vars["localRequestPath"] = form.elements["local_path"].value;
					}
					
        }
			}//next
			
			//reload app
			//_runApp();
			_vars["GET"] = func.parseGetParams( _vars["init_url"] ); 
			_urlManager();
			
			$("#collapse-panel-service").collapse("hide");
			//_vars["panelService"].classList.remove("show");
			//_vars["panelService"].classList.add("hide");
			
			return false;
		};//end event
*/

/*
	document.onkeydown = function(event) {
		event = event || window.event;
		var keyCode =  event.which ||  event.keyCode;		
//console.log(keyCode);			
		if( keyCode === 27 || keyCode === 192 ){//ESC or tilda
			var log = getById("log-wrap");
			if( log.style.display === "none"){
				log.style.display="block";
			} else {
				log.style.display="none";
			}
//console.log(log.style.display);			
		}
	};//end event

*/

	}//end _defineEvents()
	
	
	
	function _urlManager( target ){
//console.log(target, _vars["GET"]);

		switch( _vars["GET"]["q"] ) {

			case "toggle-log":
//console.log(webApp.vars["log"]..style.display);
				if( _vars["log"].style.display==="none"){
					_vars["log"].style.display="block";
					_vars["btnToggle"].innerHTML="-";
				} else {
					_vars["log"].style.display="none";
					_vars["btnToggle"].innerHTML="+";
				}
			break;
		
			case "clear-log":
				_vars["log"].innerHTML="";
			break;

			case "test":
				var funcName = _vars["GET"]["func"];
				window[funcName]();
			break;
						
			case "load-local-file":
			case "load-xml":
				loadXml();
			break;
//"update-cache"			
			
			default:
console.log("function _urlManager(),  GET query string: ", _vars["GET"]);			
			break;
		}//end switch
		
	}//end _urlManager()


	function _loadData( opt ){
//console.log("_loadData() ", arguments);
		var p = {
			"postFunc": null,
			"from_code" : $("#from-title").data("code"),
			"to_code" : $("#to-title").data("code"),
			"date": $("#date-widget").val()//2019-04-26
		};
		//extend options object
		for(var key in opt ){
			p[key] = opt[key];
		}
//console.log(p);

		if( !p.date ||  p.date.length === 0){
webApp.vars["logMsg"] = "error, empty or wrong date...";
func.logAlert( webApp.vars["logMsg"], "error");
console.log( webApp.vars["logMsg"] );
			if( typeof p["postFunc"] === "function"){
				p["postFunc"]();
			}
			return false;
		}
/*
		if( !p.from_code ||  p.from_code.length === 0){
webApp.vars["logMsg"] = "error, empty or from_code...";
func.logAlert( webApp.vars["logMsg"], "error");
console.log( webApp.vars["logMsg"] );
			if( typeof p["postFunc"] === "function"){
				p["postFunc"]();
			}
			return false;
		}
		
		if( !p.to_code ||  p.to_code.length === 0){
webApp.vars["logMsg"] = "error, empty or to_code...";
func.logAlert( webApp.vars["logMsg"], "error");
console.log( webApp.vars["logMsg"] );
			if( typeof p["postFunc"] === "function"){
				p["postFunc"]();
			}
			return false;
		}
*/		
		
		var dataUrl = webApp.vars["DB"]["dataUrl"]
		.replace("{{from_code}}", p["from_code"] )
		.replace("{{to_code}}", p["to_code"] )
		.replace("{{apikey}}", webApp.vars["apiKey"] )
		.replace("{{date}}", p.date );
console.log( dataUrl );		
//return;

		func.runAjax( {
			"requestMethod" : "GET", 
			"url" :  dataUrl, 
			
			"onProgress" : function( e ){
				var percentComplete = 0;
				if(e.lengthComputable) {
					percentComplete = Math.ceil(e.loaded / e.total * 100);
				}
console.log( "Loaded " + e.loaded + " bytes of total " + e.total, e.lengthComputable, percentComplete+"%" );
				if( webApp.vars["loadProgressBar"] ){
					webApp.vars["loadProgressBar"].className = "progress-bar";
					webApp.vars["loadProgressBar"].style.width = percentComplete+"%";
					webApp.vars["loadProgressBar"].innerHTML = percentComplete+"%";
					
					webApp.vars["numTotalLoad"].innerHTML = ((e.total / 1024) / 1024).toFixed(2)  + " Mb";
				}
				
			},
				
			"onLoadEnd" : function( headers ){
//console.log( headers );
			},
			
			"onError" : function( xhr ){
//console.log( "onError ", arguments);
webApp.vars["logMsg"] = "error, ajax load failed..." + webApp.vars["DB"]["dataUrl"];
func.logAlert( webApp.vars["logMsg"], "error");
				for( var key in e){
					webApp.vars["logMsg"] = "<b>"+key +"</b> : "+ e[key];
					func.logAlert( webApp.vars["logMsg"], "error");
//console.log( webApp.vars["logMsg"] );
				}//next
				if( typeof p["postFunc"] === "function"){
					p["postFunc"]();
				}
				//return false;
			},

			"callback": function( data, runtime, xhr ){
//console.log( "runAjax, ", typeof data );
//console.log( data );
//for( var key in data){
//console.log(key +" : "+data[key]);
//}
				_parseAjax({
					"responseType": xhr.getResponseHeader('content-type'),
					"response": data
				});

				if( typeof p["postFunc"] === "function"){
					p["postFunc"]();
				}
			}//end callback()
		});

		//return false;
		
	}//end _loadData()
	
/*
function _loadData( postFunc ){
//console.log("_loadData() ", arguments);
	//if( !webApp.iDBmodule.dbInfo["allowIndexedDB"] ){
		webApp.vars["dataStoreType"] = false;
	//} 
		switch(webApp.vars["dataStoreType"]) {				
			case "indexedDB":
			break;
			
			case "webSQL":
			break;
			
			case "localStorage":
			break;

			default:
				if( !webApp.vars["DB"]["dataUrl"] ||
					webApp.vars["DB"]["dataUrl"].length === 0 ){
webApp.vars["logMsg"] = "error, not found or incorrect 'dataUrl'...";
func.logAlert( webApp.vars["logMsg"], "error");
console.log( webApp.vars["logMsg"] );
					return false;
				}

				func.runAjax( {
					"requestMethod" : "GET", 
					"url" : webApp.vars["DB"]["dataUrl"], 
					
					"onProgress" : function( e ){
						var percentComplete = 0;
						if(e.lengthComputable) {
							percentComplete = Math.ceil(e.loaded / e.total * 100);
						}
console.log( "Loaded " + e.loaded + " bytes of total " + e.total, e.lengthComputable, percentComplete+"%" );
						if( webApp.vars["loadProgressBar"] ){
							webApp.vars["loadProgressBar"].className = "progress-bar";
							webApp.vars["loadProgressBar"].style.width = percentComplete+"%";
							webApp.vars["loadProgressBar"].innerHTML = percentComplete+"%";
							
							webApp.vars["numTotalLoad"].innerHTML = ((e.total / 1024) / 1024).toFixed(2)  + " Mb";
						}
						
					},
						
					"onLoadEnd" : function( headers ){
//console.log( headers );
					},
					
					"onError" : function( xhr ){
//console.log( "onError ", arguments);
webApp.vars["logMsg"] = "error, ajax load failed..." + webApp.vars["DB"]["dataUrl"];
func.logAlert( webApp.vars["logMsg"], "danger");
console.log( webApp.vars["logMsg"] );
						if( typeof postFunc === "function"){
							postFunc();
						}
						//return false;
					},

					"callback": function( data ){

webApp.vars["logMsg"] = "Load data file " + webApp.vars["DB"]["dataUrl"];
func.logAlert( webApp.vars["logMsg"], "success");
//console.log( webApp.vars["logMsg"] );

//console.log( "runAjax, ", typeof data );
//console.log( data );
//for( var key in data){
//console.log(key +" : "+data[key]);
//}
						if( !data ){
webApp.vars["logMsg"] = "error, no data in " + webApp.vars["DB"]["dataUrl"];
func.logAlert( webApp.vars["logMsg"],  "danger");
console.log( webApp.vars["logMsg"] );
							if( typeof postFunc === "function"){
								postFunc(false);
							}
							return false;
						}

						_parseAjax( data );

						if( typeof postFunc === "function"){
							postFunc();
						}

					}//end callback()
				});

			break;
		}//end switch
		
		//return false;
		
		function _parseAjax( data ){
			if( webApp.vars["DB"]["dbType"].length === 0 ){
webApp.vars["logMsg"] = "error, no found or incorrect " + webApp.vars["DB"]["dbType"];
//func.logAlert( webApp.vars["logMsg"], "danger");
console.log( webApp.vars["logMsg"] );
				return false;
			}
			
			switch( webApp.vars["DB"]["dbType"] ){
				case "xml":
					_parseXML( data );
				break;
				
				case "json":
				break;
				
				case "csv":
					//_parseCSVBlocks(data);
				break;
			}//end switch
			
		}//_parseAjax()
		
	}//end _loadData()
*/


	function _parseAjax( opt ){
		
		var p = {
			"responseType": null,
			"response": false,
			"postFunc": null
		};
		//extend options object
		for(var key in opt ){
			p[key] = opt[key];
		}
//console.log(p);

		if( !p.response ||  p.response.length === 0){
webApp.vars["logMsg"] = "error, empty or wrong response data...";
func.logAlert( webApp.vars["logMsg"], "error");
console.log( webApp.vars["logMsg"] );
			if( typeof p["postFunc"] === "function"){
				p["postFunc"]();
			}
			return false;
		}

		var parseData = false;
		if( p.responseType.indexOf("application/xml") !== -1){
			parseData = _parseXML( p.response );
		}
		
		if( p.responseType.indexOf("application/json") !== -1){
			parseData = _parseJSON( p.response );
		}

		if( typeof p["postFunc"] === "function"){
			p["postFunc"]( parseData );
		}
		
	}//_parseAjax()



//========================= SERVER requests
	function checkForm(opt){
		var p = {
			"form" : null
		};
		
		//extend options object
		for(var key in opt ){
			p[key] = opt[key];
		}
//console.log( p );

		if( !p["form"] ){
_vars["logMsg"] = "error, checkForm()";
func.logAlert(_vars["logMsg"], "error");
			return false;
		}

		var form = p["form"];

		var res = true;
		for( var n = 0; n < form.elements.length; n++){
			var _element = form.elements[n];
				
			if( _element.classList.contains("invalid-field") ){
				_element.classList.remove("invalid-field");
			}	
			//if( _element.type === "text" ){
				//if( _element.className.indexOf("require-form-element") !== -1 ){
				if( _element.classList.contains("require-form-element") ){
//console.log( _element.value );
					//_element.className.replace("invalid-field", "").trim();
					if( _element.value.length === 0 ){
						res = false;
_vars["logMsg"] = "error, empty required input field <b>'" + _element.name +"'</b>";
func.logAlert( _vars["logMsg"], "error");
						//_element.className += " invalid-field";
						_element.classList.add("invalid-field")
//console.log( _element.className );
//console.log( _element.classList );
						//break;
					}
				}
			//}
				
		}//next
		
		return res;
	}//end checkForm()


	function sendForm( opt ){
		var p = {
			"form": null,
			"url" : _vars["serverUrl"],
			"postFunc": null
		};
		//extend options object
		for(var key in opt ){
			p[key] = opt[key];
		}
console.log( p );
		
		if( !p["form"] ){
_vars["logMsg"] = "error, sendForm()";
func.logAlert(_vars["logMsg"], "error");
			return false;
		}

		//var formData = new FormData( form );
		//for (var pair of formData.entries()) {
//console.log(pair[0]+ ', ' + pair[1]);
		//}

		var formValues = $(p.form).serialize();
console.log( formValues );

//https://api.jquery.com/jquery.post/
		var dataType = "json";//xml, json, script, text, html
		$.post(	
				p.url, 
				formValues, 
				function(data, textStatus, jqXHR){
console.log(arguments);
					if( typeof p.postFunc === "function"){
						p.postFunc(data);
					}
				},
				dataType
		);
			
	}//end sendForm()

/*
	function sendRPC( opt ){

		var p = {
			"action": "",//"get_content_list",
			"url" : _vars["serverUrl"],
			"postFunc": null
		};
		
		//extend options object
		for(var key in opt ){
			p[key] = opt[key];
		}
//console.log( p );

		//if( !p["parent_id"]){
			//return false;
		//}
		
		var url = false;
		switch( p.action ){
			
			case "get_content_list":
				url = p.url + "?q=content/rpc_list";
			break;
			
			case "get_booklist":
				url = p.url + "?q=content/rpc_booklist";
			break;
			
			case "get_content_item":
				url = p.url + "?q=content/rpc_get_item&id="+p.id;
			break;
			
			case "remove_content_item":
				url = p.url + "?q=content/rpc_remove&id="+p.id;
			break;
			
			case "save_content_item":
				//url = p.url + "?q=content/rpc_remove&id="+p.id;
				sendForm({
					"form": document.forms["form_node"],
					"url" : p.url + "?q=content/rpc_save",
					"postFunc": function(_resp){
//_vars["logMsg"] = "send form to server: " + _vars["serverUrl"];
//func.logAlert(_vars["logMsg"], "info");
console.log(_resp);
						if( typeof p.postFunc === "function"){
							p.postFunc( _resp );
						}
					} 
				});
				return;
			break;
			
			case "get_widget_content_links":
				url = p.url + "?q=content-links/rpc_widget_content_links";
			break;
			
			//default:
			//break;
		}//end switch
		
		if( !url ){
_vars["logMsg"] = "error, sendRPC(), wrong RPC action: " + p.action;
func.logAlert(_vars["logMsg"], "error");
console.log(_vars["logMsg"]);
			return false;
		}
		
		//start block window
		if( webApp["vars"]["waitWindow"] ){
			webApp["vars"]["waitWindow"].style.display="block";
		}

		$.getJSON(  url, function( resp ){
//console.log(resp);
			if( typeof p.postFunc === "function"){
				p.postFunc( resp );
			}
		})
		.done(function () {
_vars["logMsg"] = "$.getJSON, done, url: " + url;
func.logAlert(_vars["logMsg"], "success");
//console.log( arguments );
		})
		.fail(function (xhr, textStatus, error) {
_vars["logMsg"] = "$.getJSON, "+textStatus+", "+error+", url: " + url;
func.logAlert( _vars["logMsg"], "error");
//console.log( _vars["logMsg"], arguments );
			if( typeof p.postFunc === "function"){
				var resp = {
					"eventType": "error", 
					"data": []
				};
				p.postFunc(resp);
			}
			
		});

	}//end sendRPC()
*/

	function sendRequest( opt ){
		var p = {
			"dataUrl" : false,
			"apiKey" : false,
			"apiName" : false,
			"callback" : function(){
	//console.log(arguments);
			}//end callback
		};
	//console.log(opt);
		//extend p object
		for(var key in opt ){
			p[key] = opt[key];
		}
//console.log(p);

		var dataUrl = p["dataUrl"];
		try{
			var xhr = new XMLHttpRequest();
			xhr.open("GET", dataUrl, true);
			if(p.apiName === "weatherAPI"){
				xhr.setRequestHeader("X-Yandex-API-Key", p["apiKey"] );
				//xhr.setRequestHeader("Access-Control-Allow-Credentials", "true");
			}
			xhr.onload = function(e) {
	console.log(arguments);
	// console.log("event type:" + e.type);
	console.log("time: " + e.timeStamp);
	console.log("total: " + e.total);
	console.log("loaded: " + e.loaded);	
	//console.log( this.responseText );
	//func.log( this.responseText, "response");
				
				var _response = false;
				if( this.responseText.length > 0 ){
					_response = this.responseText;
					webApp.vars["responseType"] = xhr.getResponseHeader('content-type');
				}
				
				if( typeof p.callback === "function"){
					p.callback(_response);
				}
			}//end onload
			
			xhr.onerror = function(e) {
//console.log(arguments);		
console.log(e);		

	webApp.vars["logMsg"] = "error, ajax load failed...";
	//func.logAlert( _vars["logMsg"], "error");
	func.logAlert( webApp.vars["logMsg"], "danger");
	
				for( var key in e){
//console.log( key +" : "+e[key] );
					webApp.vars["logMsg"] = "<b>"+key +"</b> : "+ e[key];
					func.logAlert( webApp.vars["logMsg"], "error");
				}//next
					
				p.callback(false);
			}//end error callback
			
			xhr.onloadend = function(e){
	//console.log(xhr.getResponseHeader('X-Powered-By') );
	console.log( xhr.getAllResponseHeaders() );
			}//end onloadend
			
			xhr.send();
			
		} catch(e){
	console.log(e);	
		}	
		
	}//end sendRequest()


	function _serverRequest( opt ){
		var p = {
			//"date": null,
			"callback": null
		};
		
		//extend options object
		for(var key in opt ){
			p[key] = opt[key];
		}
//console.log(p);		
/*
//---------------------- Server TESTS
		if( typeof webApp.vars["supportPHP"] === "undefined" ||
				typeof webApp.vars["supportASPX"] === "undefined"){
			__testPHP(function( supportPHP ){
console.log("supportPHP:" + supportPHP);

				if( supportPHP ){
					__processRequest();
				} else {
					
					__testASPX(function( supportASPX ){
		console.log("supportASPX:" + supportASPX);
						// if( supportASPX ){
							// __processRequest();
						// } else {
							// //next test....
						// }
						__processRequest();
						
					})//end test;
				}
				
			})//end test;
		}
//---------------------
*/
		__processRequest();

		function __processRequest(){
			
			//form url
			//var url = webApp.vars["import"]["request_url"];
			var url = webApp.vars["import"]["data_url"];
			// if( webApp.vars["supportPHP"] ){
				// url = webApp.vars["import"]["request_url_PHP"];
			// }
			// if( webApp.vars["supportASPX"] ){
				// url = webApp.vars["import"]["request_url_ASPX"];
			// }
			
			// if( p["date"] && p["date"].length > 0 ){
				// url +=  "?date="+p["date"];
			// }
			//var url = webApp.vars["import"]["data_url"] + "?date="+p["date"];
			
			runAjax( {
				"requestMethod" : "GET", 
				//"requestMethod" : "HEAD", 
				"url" : url, 
				"onProgress" : function(e){
					var percentComplete = 0;
					if(e.lengthComputable) {
						percentComplete = Math.ceil(e.loaded / e.total * 100);
						if( webApp.vars["loadProgress"] ){
							webApp.vars["loadProgress"].value = percentComplete;
						}
						if( webApp.vars["loadProgressBar"] ){
							webApp.vars["loadProgressBar"].className = "progress-bar";
							webApp.vars["loadProgressBar"].style.width = percentComplete+"%";
							webApp.vars["loadProgressBar"].innerHTML = percentComplete+"%";
						}

					}
console.log( "Loaded " + e.loaded + " bytes of total " + e.total, e.lengthComputable, percentComplete+"%" );

					
				},//end onProgress()
				"callback": function( data ){
var msg = "load " + url ;
console.log(msg);

					if( !data || data.length === 0){
	console.log("error in _app(), _serverRequest(), not find 'data'.... ");			
						data = [];
					}
					
					if( typeof p["callback"] === "function"){
						p["callback"](data);
						return false;
					} 
					
				}//end callback()
			});
		}//end __processRequest()
/*
		function __testPHP( callback ){
			runAjax( {
				"requestMethod" : "GET", 
				"url" : webApp.vars["testUrlPHP"], 
				"callback": function( data ){
//console.log(data, typeof data, data.length, data[0]);

					if( !data || data.length === 0){
console.log("error in _app(), _serverRequest(), not find 'data'.... ");			
						data = [];
					}
					
					webApp.vars["supportPHP"] = false;
					if (data[0] === "4"){//test success, result of adding 2+2 on PHP
						webApp.vars["testPHP"] = true;
					}
					
					if( typeof callback === "function"){
						callback( webApp.vars["supportPHP"] );
						return false;
					} 
					
				}//end callback()
			});
		}//end __testPHP()
		
		function __testASPX( callback ){
			runAjax( {
				"requestMethod" : "GET", 
				"url" : webApp.vars["testUrlASPX"], 
				"callback": function( data ){
//console.log(data, typeof data, data.length, data[0]);

					if( !data || data.length === 0){
console.log("error in _app(), _serverRequest(), not find 'data'.... ");			
						data = [];
					}
					
					webApp.vars["supportASPX"] = false;
					if (data[0] === "4"){//test success, result of adding 2+2 on ASPX
						webApp.vars["supportASPX"] = true;
					}
					
					if( typeof callback === "function"){
						callback( webApp.vars["supportASPX"] );
						return false;
					} 
					
				}//end callback()
			});
		}//end __testASPX()
*/		
	}//end _serverRequest()
	

//================================= PARSE data response
/*
	function parseServerResponse( opt ){
		var p = {
			"eventType": "error",
			"errorCode": null,
			"msg": null,
			"data" : null,
			"action" : false
		};
		
		//extend options object
		for(var key in opt ){
			p[key] = opt[key];
		}
//console.log( p );

		//_vars["logMsg"] = "end rpc_request, status: " + p["eventType"];
		//func.logAlert(_vars["logMsg"], p["eventType"] );

		//clear block window
//setTimeout(function(){
		if( webApp["vars"]["waitWindow"] ){
			webApp["vars"]["waitWindow"].style.display="none";
		}		
//}, 1000*3);
		
		if( p["eventType"] === "error" ){
console.log( p );

_vars["logMsg"] = "end rpc_request, status: " + p["eventType"];
func.logAlert(_vars["logMsg"], p["eventType"] );

			if( p["errorCode"] === "data_not_found" ){
_vars["logMsg"] = p["msg"];
_vars["logMsg"] += ", server return empty json object, trying to load local XML file";
func.logAlert(_vars["logMsg"], "warning");
				//_vars["serverUrl"] = false;
				_vars["dataSourceType"] = "load-local-file";
				_vars["GET"] = func.parseGetParams("?q=load-local-file"); 
				_urlManager();
			}
			return false;
		}


		if( !p["action"] ){
console.log( p );
_vars["logMsg"] = "error, undefined RPC action, parseServerResponse()";
func.logAlert(_vars["logMsg"], "error");
			return false;
		}


		switch( p.action ){
			
			case "get_booklist":
				if( p["data"] && p["data"].length > 0){
					var html = _formPageList({
						"pages" : p["data"],
						"html": _vars["templates"]["tpl-booklist"]
					});
					_vars["contentList"].innerHTML = html;
				}
			break;

			case "draw_content_item":
				if( p["data"]){

//---------------------
					//convert child nodes key content_id -> id????????????????
					if( p["data"]["child_nodes"] && 
							p["data"]["child_nodes"].length > 0
						){
						var child_nodes = p["data"]["child_nodes"];
//console.log( "test.....",child_nodes );
						for( var n = 0; n < child_nodes.length; n++){
							child_nodes[n]["id"] = child_nodes[n]["content_id"];
							delete child_nodes[n]["content_id"];
							delete child_nodes[n]["parent_id"];
						}//next
					} else {
						p["data"]["child_nodes"] = [];
					}
//---------------------

					var html = _formNode({"node" : p["data"]});
					_vars["contentList"].innerHTML = html;
						
				} else {
console.log( p );
_vars["logMsg"] = "Not find node, id:" + p.id;
func.logAlert(_vars["logMsg"], "error");
console.log( _vars["logMsg"] );
				}
			break;


			case "set_form_node":
			
				if( p["data"]){
					updateFormNode( p["data"] );
					
					_vars["contentList"].style.display = "none";
					//var nodeBody = func.getById("node-"+p["data"]["id"]);
					//nodeBody.style.display = "none";
					
					$(_vars["formNode"]).collapse("show");
					var title = _vars["editNodeTitle"];
					setWindowTitle( title );
					
				} else {
console.log( p );
_vars["logMsg"] = "Not find node, id:" + p.id;
func.logAlert(_vars["logMsg"], "error");
console.log( _vars["logMsg"] );
				}
			break;

			case "get_widget_content_links":
				if( p["data"] && p["data"].length > 0){
					return p["data"];
				} else {
console.log( p );
_vars["logMsg"] = "empty widget_content_links";
func.logAlert(_vars["logMsg"], "warning");
console.log( _vars["logMsg"] );
				}
			break;


			case "remove_content_item":
				_vars["logMsg"] = p["message"];
				func.logAlert(_vars["logMsg"], p["eventType"] );
				
				_vars["GET"] = func.parseGetParams( "?q=book-list" ); 
				_urlManager();
			break;

			case "save_content_item":
				_vars["logMsg"] = p["message"];
				func.logAlert(_vars["logMsg"], p["eventType"] );
			break;

			
			default:
console.log( p );
_vars["logMsg"] = "error, unknown RPC action: " + p.action +", parseServerResponse()";
func.logAlert(_vars["logMsg"], "error");
				return false;
			break;
		}//end switch

		
	}//end parseServerResponse()
*/

	function _parseXML(xml){
//console.log("function _parseXML()");

var timeStart = new Date();

		try{
			xmlObj = func.convertXmlToObj( xml );
//console.log(xmlObj);
delete xml;
			webApp.vars["DB"]["nodes"] = _data_formNodesObj(xmlObj);
//delete xmlObj;
			
			//_vars["hierarchyList"] = __formHierarchyList();
			//webApp.vars["loadDataRes"] = true;
var timeEnd = new Date();
var runTime = (timeEnd.getTime() - timeStart.getTime()) / 1000;
webApp.vars["logMsg"] = "- convertXmlToObj(), runtime: <b>" + runTime  + "</b> sec";
func.logAlert( webApp.vars["logMsg"], "info");
console.log( webApp.vars["logMsg"] );

		} catch(error) {
webApp.vars["logMsg"] = "convertXmlToObj(), error parse XML..." ;
func.logAlert( webApp.vars["logMsg"], "error");
console.log( error );
		}//end catch

	}//end _parseXML()


	function _parseJSON( jsonStr ){
		try{
			var jsonObj = JSON.parse( jsonStr, function(key, value) {
	//console.log( key, value );
				return value;
			});
//console.log( jsonObj );
	
			return jsonObj;
			
		} catch(error) {
webApp.vars["logMsg"] = "error, error JSON.parse server response data...." ;
console.log( error );
func.logAlert( webApp.vars["logMsg"], "error");
			return;
		}//end catch

	}//end _parseJSON()


	function _parseLocalFile( fileList){
		if( !fileList || fileList.length === 0){
			return false;
		}
/*
name protokols.json
lastModified 1515750341802
lastModifiedDate Date 2018-01-12T09:45:41.802Z
webkitRelativePath 
slice function slice()
size 47
type application/json
*/
		for( var n = 0; n < fileList.length; n++){
			var file = fileList[n];
			for(var key in file){
console.log(key, file[key]);	
			}//next
			__processFile(file);
		}//next
		
		function __processFile(file){
			//check file type
			//webApp.logMsg = "file type:" + file["type"];
			//func.log("<div class='alert alert-info'>" + webApp.logMsg + "</div>");
			
			var reader = new FileReader();
			
			reader.onabort = function(e){
console.log( "reader, onabort", e );
			};
			
			reader.onerror = function(e){
console.log( "reader, onerror", e );
			};
			
			reader.onload = function(e){
console.log( "reader, onload" );
//console.log(e.target.result);
				_parseJSON( e.target.result );

				webApp.logMsg = "Load file " + file.name;
				webApp.logMsg += ", size: " + file.size;
				webApp.logMsg += ", type: " + file.type;
				webApp.logMsg += ", date: " + file.lastModifiedDate;
	
//need new func!!!!!!!!!!!!!	
				var timestamp = file.lastModified;
				var date = new Date();
				date.setTime( timestamp);
//console.log( date );
				var sYear = date.getFullYear();
				var sMonth = date.getMonth() + 1;
				var sDate = date.getDate();
				var sHours = date.getHours();
				var sMinutes = date.getMinutes();
				var dateStr = sYear + "-" + sMonth + "-" + sDate + " " + sHours + ":" + sMinutes;

				webApp.logMsg += ", date2: "+ dateStr;
				func.logAlert( webApp.logMsg, "info");
				
//----------------------
				hideModalWindow();
				
			};
			
			reader.onloadstart = function(e){
console.log( "reader, loadstart" );
			};
			
			reader.onloadend = function(e){
console.log( "reader, loadend" );
			};
			
			reader.onprogress = function(e){
console.log( "reader, progress");
			};
			
			reader.readAsText(file);
		}//end __processFile()
		
	}//end _parseLocalFile


/*
		function _loadLocalFile( fileList){
			if( !fileList || fileList.length === 0){
				return false;
			}

	// name protokols.json
	// lastModified 1515750341802
	// lastModifiedDate Date 2018-01-12T09:45:41.802Z
	// webkitRelativePath 
	// slice function slice()
	// size 47
	// type application/json

			for( var n = 0; n < fileList.length; n++){
				var file = fileList[n];
				for(var key in file){
console.log(key, file[key]);	
				}//next
				__processFile(file);
			}//next
			
			function __processFile(file){
				
				//check file type
				if ( file.type !== "text/xml") {
					_vars.logMsg = "Skip file, incorrect file type, only XML... " + file.name +",  " +file.type;
					func.log("<div class='alert alert-warning'>" + _vars.logMsg + "</div>");
					return false;
				}
				
_vars["timeStart"] = new Date();
if( _vars["waitWindow"] ){
	_vars["waitWindow"].style.display="block";
}
					
				var reader = new FileReader();
				
				reader.onabort = function(e){
console.log( "reader, onabort", e );
				};
				
				reader.onerror = function(e){
console.log( "reader, onerror", e );
				};
				
				reader.onload = function(e){
console.log( "reader, onload" );
//console.log(e.target.result);
					_import({
						"xml": e.target.result,
						"fileName": file.name,
						"timeStart": _vars["timeStart"]
					});

					_vars.logMsg = "Load file <b>" + file.name + "</b>";
					_vars.logMsg += "<br> size: <b>" + file.size +"</b> bytes";
					_vars.logMsg += "<br> type: <b>" + file.type +"</b>";
					
					var timestamp = file.lastModified;
					var date = new Date();
					date.setTime( timestamp );
//console.log( timestamp, date );
					_vars.logMsg += "<br/> date: " + date;
		

					var sYear = date.getFullYear();
					
					var sMonth = date.getMonth() + 1;
					if(sMonth < 10){
						sMonth = "0"+sMonth;
					}
					
					var sDate = date.getDate();
					
					var sHours = date.getHours();
					if(sHours < 10){
						sHours = "0"+sHours;
					}
					
					var sMinutes = date.getMinutes();
					if(sMinutes < 10){
						sMinutes = "0"+sMinutes;
					}
					
					var dateStr = sYear + "-" + sMonth + "-" + sDate + " " + sHours + ":" + sMinutes;

					_vars.logMsg += "<br/> date2: "+ dateStr;
					
					func.log("<div class='alert alert-success'>" + _vars.logMsg + "</div>");
				};
				
				reader.onloadstart = function(e){
console.log( "reader, loadstart" );
				};
				
				reader.onloadend = function(e){
console.log( "reader, loadend" );
				};
				
				reader.onprogress = function(e){
console.log( "reader, progress");
				};
				
				reader.readAsText(file);
			}//end __processFile()
			
		}//end _loadLocalFile
*/

//============================== select DATA
/*
	function _formQueryObj(opt){
		var p = {
			queryTarget : "",//"getTerminByName"
			vocName : "",//"info",
			termName : "",//"жанр"
			"vid" : null,//"5" (info vocabulary)
			"tid" : null,//"97" (техника)
			"nid" : null//"20" (Ballet Mistress)
		};
		//extend p object
		for(var key in opt ){
			p[key] = opt[key];
		}
//console.log(p);

		if( p["queryTarget"].length === 0 ){
console.log("error in _formQueryObj(), empty 'queryTarget'....");
			return false;
		}

		switch( p.queryTarget ) {
			
			case "getTerminByName":
				if( p["vocName"].length === 0 ){
console.log("error in _formQueryObj(), empty 'vocabularyName'....");
					return false;
				}
				if( p["termName"].length === 0 ){
console.log("error in _formQueryObj(), empty 'termName'....");
					return false;
				}
				return __formQueryTerminByName();
			break;
			
			case "getChildTerms":
				if( p["vid"].length === 0 ){
console.log("error in _formQueryObj(), empty 'vid'....");
					return false;
				}
				if( p["tid"].length === 0 ){
console.log("error in _formQueryObj(), empty 'tid'....");
					return false;
				}
				return __formQueryChildTerms();
			break;
			
			case "getNodeTerms":
				if( p["nid"].length === 0 ){
console.log("error in _formQueryObj(), empty 'nid'....");
					return false;
				}
				return __formQueryNodeTerms();
			break;
			
		}//end switch
		
		function __formQueryNodeTerms(){
			var subQuery1 = {
				"action" : "select",
				"tableName": "term_node",
				"targetFields" : ["tid"],
				"where" : [
					{"key" : "nid", "value" : p["nid"], "compare": "="}
				]
			};
			var baseQuery = {
				"action" : "select",
				"tableName": "term_data",
				"targetFields" : [
"tid",
"vid",
"name"//,
//"description",
//"weight"
],
				"where" : [
					{"key" : "tid", "compare": "=", "value" : subQuery1}
				]
			};
//console.log(baseQuery);
			return baseQuery;
		}//end __formQueryNodeTerms()

		
		function __formQueryChildTerms(){
			var subQuery1 = {
				"action" : "select",
				"tableName": "term_hierarchy",
				"targetFields" : ["tid"],
				"where" : [
						{"key" : "parent", "value" : p["tid"], "compare": "="}
					]
			};
			var baseQuery = {
				"action" : "select",
				"tableName": "term_data",
				"targetFields" : [
"tid",
"vid",
"name"//,
//"description",
//"weight"
],
					"where" : [
						{"key" : "vid", "compare": "=", "value" : p["vid"]},
						{"logic": "AND", "key" : "tid", "compare": "=", "value" : subQuery1}
					]
				};

			// webApp.db.query({
				// "queryObj" : baseQuery,
				// "callback" : function( res ){
// console.log(res);
				// }//end callback
			// });
			return baseQuery;
		}//end __formQueryChildTerms()
		
		function __formQueryTerminByName(){

		////form data queries
	////1. select vid from vocabulary where name="info" -- 5
	////1. select tid from term_data where name="жанр" -- 95
	////2. select tid from term_hierarchy where parent=95 -- "100", "101", "102", "104", "111", "113", "114", "132", "149", "176", "178", "187", "196", "226"
	////3. select name from term_data where vid=5 and tid in ("100", "101", "102", "104", "111", "113", "114", "132", "149", "176", "178", "187", "196", "226")
	////4. select dst from url_alias where src IN ("taxonomy/term/100", "taxonomy/term/101".....)
////test subQuery!!!!!		
				//// var queryStr = "\
	//// select name from term_data where vid=(\
		//// select vid from vocabulary where name='info'\
	//// ) and tid in (\
		//// select tid from term_hierarchy where parent=(\
			//// select tid from term_data where name='жанр'\
		//// )\
	//// )";

			var subQuery1 = {
				"action" : "select",
				"tableName": "vocabulary",
				"targetFields" : ["vid"],
				"where" : [
					{"key" : "name", "compare": "=", "value" : p.vocName}
				]
			};
			
			var subQuery3 = {
				"action" : "select",
				"tableName": "term_data",
				"targetFields" : ["tid"],
				"where" : [
					{"key" : "name", "compare": "=", "value" : p.termName}
				]
			};
			var subQuery2 = {
				"action" : "select",
				"tableName": "term_hierarchy",
				"targetFields" : ["tid"],
				"where" : [
					{"key" : "parent", "compare": "=", "value" : subQuery3}
					//{"key" : "parent", "compare": "=", "value" : 95}
				]
			};

			var baseQuery = {
					"action" : "select",
					"tableName": "term_data",
					"targetFields" : [
	"tid",
	"vid",
	"name"//,
	//"description",
	//"weight"
	],
					"where" : [
						{"key" : "vid", "compare": "=", "value" : subQuery1},
						{"logic": "AND", "key" : "tid", "compare": "=", "value" : subQuery2}
					]
				};
				
			//_vars["queries"]["getTermGenre"] = baseQuery;
//console.log(baseQuery);
			return baseQuery;
		}//end __formQueryTerminByName()
		
	}//end _formQueryObj()
	
*/
 
//============================================== DRAW
	function _draw_wrapData( opt ){
		var p = {
			"data": null,
			//"type" : "",
			//"wrapType" : "menu",
			"templateID" : false,
			"templateListItemID": false
		};
		//extend options object
		for(var key in opt ){
			p[key] = opt[key];
		}
//console.log(p);

		if( !p["data"] || p["data"].length === 0){
console.log("-- _draw_wrapData(), error, incorrect data ...");
			return false;
		}
		if( !p["templateID"] ){
console.log("-- _draw_wrapData(), error, templateID was not defined...");
			return false;
		}
		
		if( !webApp.vars["templates"][p.templateID] ){
webApp.vars["logMsg"] = "-- _draw_wrapData(),  error, not find template, id: " + p.templateID;
func.logAlert( webApp.vars["logMsg"], "warning");
console.log(webApp.vars["logMsg"]);
			return false;
		}
		
		var html = "";
//console.log( p["data"].length );

		p["wrapType"] = "item";
		if( p["data"].length > 0 ){
			p["wrapType"] = "list";
		}
		switch( p["wrapType"] ){
			case "item" :
				html = __formNodeHtml( p["data"], webApp.vars["templates"][ p.templateID ] );
			break;
			case "list" :
				if( !p["templateListItemID"] ){
webApp.vars["logMsg"] = "-- wrapData(), error, var templateListItemID incorrect...";
console.log(webApp.vars["logMsg"]);							
					return false;
				}
				html = __formListHtml( webApp.vars["templates"][ p.templateID ] );
			break;
		}//end switch
		
//console.log(html);
		return html;

		function __formNodeHtml( data, _html ){
			
			for( var key in data ){
//console.log(key, data[key]);
				if( _html.indexOf("{{"+key+"}}") !== -1 ){
//console.log(key, data[key]);
					_html = _html.replace( new RegExp("{{"+key+"}}", "g"), data[key] );
				}
			}//next
			
//--------------- clear undefined keys (text between {{...}} )
_html = _html.replace( new RegExp(/{{(.*?)}}/g), "");
//--------------------			

			return _html;
		}//end __formNodeHtml()
		
		function __formListHtml( _html ){
			
			var listHtml = "";
			for( var n = 0; n < p["data"].length; n++){
//console.log( n );
//console.log( p["data"][n], typeof p["data"][n], p["data"].length);

				//form list items
				var item = p["data"][n];
					
				//var itemTpl = _vars["templates"][ p.templateListID];
				//var itemHtml = __formNodeHtml( item, itemTpl );
				
				var itemHtml = webApp.vars["templates"][ p.templateListItemID];
				
				
				//load unique template for item
				if( item["template"] && item["template"].length > 0){
					var tplName = item["template"];
					if( webApp.vars["templates"][ tplName ] ){
						itemHtml = webApp.vars["templates"][ tplName ];
					} else {
console.log("-- warning, not found template, ", tplName );
					}
				}

//--------------- get keys from template (text between {{...}} )
				//if(n === 1){
					var tplKeys = itemHtml.match(/{{(.*?)}}/g);
					for(var n1 = 0; n1 < tplKeys.length; n1++){
						tplKeys[n1] = tplKeys[n1].replace("{{","").replace("}}","");
					}//next
//console.log( tplKeys, p.templateListItemID, item );
				//}
//---------------

				//make copy object item
				//var _tmp = {
					//"number": item["number"]
				//};
				var jsonNode = JSON.stringify( item );
				var _tmp = JSON.parse( jsonNode);
				
				//for( var key2 in item){
				for( var n1 = 0; n1 < tplKeys.length; n1++){
					var key2 = tplKeys[n1];
//console.log(item[key2] instanceof Array, key2, item[key2]);
//if(n === 1){
//console.log(key2, item[key2]);
//}

					if( item[key2] instanceof Array ){
						if(item[key2].length === 0){
console.log("-- warning, empty field....", key2, item[key2]);
//continue;	
							item[key2] = "<span class='not-found-item'>not found " + key2 +"</span>";
						} else {
							var subOrdList = item[key2]["listTpl"];
							var itemTpl = item[key2]["itemTpl"];
	/*						
							if( key2 === "title" ){
								var subOrdList = webApp.vars["templates"]["tpl-videolist"];
								var itemTpl = webApp.vars["templates"]["tpl-videolist-item--video-title"];
							}

							if( key2 === "ul" ){
								var subOrdList = webApp.vars["templates"]["tpl-videolist-links"];
								var itemTpl = webApp.vars["templates"]["tpl-videolist-item--video-ul"];
								//var subOrdListHtml = "";
								//for( var n2 = 0; n2 < item[key2].length; n2++){
									//subOrdListHtml += __formNodeHtml( item[key2][n2], itemTpl );
								//}//next
								//subOrdList = subOrdList.replace("{{list}}", subOrdListHtml);
								//item[key2] = subOrdList;
							}

							if( key2 === "tags" ){
								var subOrdList = webApp.vars["templates"]["tpl-videolist-tags"];
								var itemTpl = webApp.vars["templates"]["tpl-videolist-item--video-tag"];
								//var subOrdListHtml = "";
								//for( var n2 = 0; n2 < item[key2].length; n2++){
									//subOrdListHtml += __formNodeHtml( item[key2][n2], itemTpl );
								//}//next
								//subOrdList = subOrdList.replace("{{list}}", subOrdListHtml);
								//item[key2] = subOrdList;
							}
							
							if( key2 === "pictures" ){
								var subOrdList = webApp.vars["templates"]["tpl-videolist-pictures"];
								var itemTpl = webApp.vars["templates"]["tpl-videolist-item--video-img"];
								//var subOrdListHtml = "";
								//for( var n2 = 0; n2 < item[key2].length; n2++){
									//subOrdListHtml += __formNodeHtml( item[key2][n2], itemTpl );
								//}//next
	////console.log( "subOrdListHtml: ", subOrdListHtml );
								//subOrdList = subOrdList.replace("{{list}}", subOrdListHtml);
	////console.log( subOrdList );
								//item[key2] = subOrdList;
							}
	*/						
							var subOrdListHtml = "";
							for( var n2 = 0; n2 < item[key2].length; n2++){
//console.log( item[key2][n2]["text"] );
								subOrdListHtml += __formNodeHtml( item[key2][n2], itemTpl );
							}//next
//console.log( subOrdListHtml );
							subOrdList = subOrdList.replace("{{list}}", subOrdListHtml);
//console.log( subOrdList );
							//item[key2] = subOrdList;
							
							//do not add HTML code to item object!!!
							_tmp[key2] = subOrdList;
						}							
					}
					
					if( itemHtml.indexOf("{{"+key2+"}}") !== -1 ){
//if(n === 1){
//console.log(key2, item[key2]);
//}						
						if( typeof item[key2] === "undefined"){
//if(n === 1){
//console.log(key2, item[key2], typeof item[key2]);
//}						
							itemHtml = itemHtml.replace(new RegExp("{{"+key2+"}}", "g"), "<span class='not-found-item'>not found " + key2 +"</span>");
						} else {
							//itemHtml = itemHtml.replace( new RegExp("{{"+key2+"}}", "g"), item[key2] );
							itemHtml = itemHtml.replace( new RegExp("{{"+key2+"}}", "g"), _tmp[key2] );
						}
					}
					
				}//next
					
				listHtml += itemHtml;
//console.log(items);
//console.log(listHtml);
			}//next
			
			_html = _html.replace("{{list}}", listHtml);
			return _html;
		}//end __formListHtml

	}//end _draw_wrapData()

	var _draw_buildBlock = function(opt){
//console.log("_buildBlock()", arguments);
		var timeStart = new Date();
		var p = {
			"title": "",
			"content" : "",
			//"contentType" : "",
			"templateID" : "tpl-block",
			"contentTpl" : "tpl-list",//"tpl-menu"
			"contentListTpl" : false,
			
			"callback" : function(){
				var timeEnd = new Date();
				var ms = timeEnd.getTime() - timeStart.getTime();
				var msg = "Generate block '" + this.title +"', "+this.templateID+", runtime:" + ms / 1000 + " sec";
console.log(msg);			
				//webApp.app.vars["runtime"].push({
					//"source" : msg,
					//"ms" : ms,
					//"sec" : ms / 1000
				//});
				
				//if( typeof p["callback2"] === "function"){
					//p["callback2"]();//return from _buildBlock()
				//}
				
			}//,//end callback
			//"callback2" : null
		};
//console.log(opt);
		//extend p object
		for(var key in opt ){
			p[key] = opt[key];
		}
//console.log(p);
		_draw_insertBlock( p );
	};//end _draw_buildBlock()


	var _draw_insertBlock = function( opt ){
		var p = {
			"templateID": false,
			"locationID": "block-1",
			"title" : "block",
			"content" : false,
			"callback":null
		};
		//extend options object
		for(var key in opt ){
			p[key] = opt[key];
		}
//console.log("_draw_insertBlock()", p);

		var templateID = p["templateID"];
		if( !webApp.vars["templates"][templateID] ){
webApp.vars["logMsg"] = "_draw_insertBlock(), error, not found template, id:" + templateID;
//func.logAlert( webApp.vars["logMsg"], "error");
console.log( "-- " + webApp.vars["logMsg"] );
			if( typeof p["callback"] === "function"){
				p["callback"]();
			}
			return false;
		}
		
		if( !p["content"] || p["content"].length === 0){
webApp.vars["logMsg"] = "_draw_insertBlock(), warning, not found or empty content block " + p["locationID"];
//func.logAlert( webApp.vars["logMsg"], "warning");
console.log( "-- "+webApp.vars["logMsg"] );
			//if( typeof p["callback"] === "function"){
				//p["callback"]();
			//}
			//return false;
		}
		
		var html = webApp.vars["templates"][templateID];
		html = html.replace("{{block_title}}", p["title"]);
		html = html.replace("{{content}}", p["content"]);
		
		var locationBlock = func.getById( p["locationID"] );
		if( locationBlock ){
			locationBlock.innerHTML = html;
		} else {
webApp.vars["logMsg"] = "error, not found block location id: " + p["locationID"];
func.logAlert( webApp.vars["logMsg"], "error");
console.log( webApp.vars["logMsg"] );
		}		
		
		if( typeof p["callback"] === "function"){
			p["callback"]();
		}

	};//end _draw_insertBlock()


//===============================================
	var _buildPage = function( opt ){
//console.log("_buildPage()", arguments);

		//if( webApp.vars["wait"] ){
			//webApp.vars["wait"].className="modal-backdrop in";
			//webApp.vars["wait"].style.display="block";
		//}
		
		var p = {
			"callback": null
		};
		//extend options object
		for(var key in opt ){
			p[key] = opt[key];
		}
//console.log(opt);

		//draw blocks
		for( var n = 0; n < webApp.vars["blocks"].length; n++){
			var _block = webApp.vars["blocks"][n];
//console.log(_block["title"]);				

			if( typeof _block["buildBlock"] === "function"){//dynamic form content
				//if( _block["visibility"]){
					_block["buildBlock"]();
					//_block["draw"] = true;
				//}
			} else {
//webApp.vars["logMsg"] = "warning, not found buld function....";
//console.log( "-- " + webApp.vars["logMsg"], _block );
					if( _block["content"] && _block["content"].length > 0 ){
						_draw_buildBlock( _block );
					}
			}
			
		}//next
		
/*
		//draw static blocks
		for( var n = 0; n < webApp.vars["blocks"].length; n++){
			var _opt = webApp.vars["blocks"][n];
// //console.log(_opt["visibility"], p["title"]);				
			if( _opt["visibility"]){
				// if( opt["visibility"].indexOf( p["title"] ) !== -1 ){
					_draw_buildBlock( _opt );
				// }
			}
			
		}//next
*/
		
/*
		for( var n = 0; n < webApp.vars["blocks"].length; n++){
			var _opt = webApp.vars["blocks"][n];
			
			//do not redraw existing block
			if( _opt["draw"] && !_opt["refresh"]){
				continue;
			}
			
			if( _opt["visibility"]){
				
				//closures, need for async data getting from indexedDB
				(function(_opt_){
					//setTimeout(function(){ 
						//console.log("-- closure function, ", _opt_); 
					//}, 1000);
					//_draw_buildBlock( _opt_ );
					
					if( typeof _opt_["buildBlock"] === "function"){
						//if( _opt_["visibility"]){
							_opt_["buildBlock"]();
							_opt_["draw"] = true;
						//}
					} else {
webApp.vars["logMsg"] = "warning, not found buld function....";
console.log( "-- " + webApp.vars["logMsg"], _opt_ );
					}
				})(_opt);//end closure
			}

		}//next
*/

		//if( webApp.vars["wait"] ){
			////webApp.vars["wait"].className="";
			//webApp.vars["wait"].style.display="none";
		//}

		if( typeof p["callback"] === "function"){//return from _buildPage()
			p["callback"]();
		}
			
	};//end _buildPage()
	
/*
					_buildBlock({//draw content block
						"name" : "block-content",
						//"title" : "termin_nodes", 
						"title" : block_title, 
						"templateID" : "tpl-block-content",
						"contentTpl" : "tpl-termin_nodes",
						"contentListTpl" : "tpl-termin_nodes_list",
						"content" : function(args){
							
							webApp.db.getTerminNodes({//get list termin nodes
								"tid" : webApp.vars["GET"]["tid"],
								"callback" : function( res ){
//console.log(res);
									if( typeof args["callback"] === "function"){
										args["callback"]( res );
									}

								}//end callback
							});
						}//end callback

*/

/*
	var _buildBlock = function(opt){
//console.log("_buildBlock()", arguments);

		var timeStart = new Date();
		var p = {
			"title": "block title",
			"content" : "",
			//"contentType" : "",
			"templateID" : "tpl-block",
			
			"contentTpl" : "tpl-list",//"tpl-menu"
			"contentListTpl" : false,
			
			"callback" : function(){
				var timeEnd = new Date();
				var ms = timeEnd.getTime() - timeStart.getTime();
				var msg = "Generate block '" + this.title +"', "+this.templateID+", runtime:" + ms / 1000 + " sec";
console.log(msg);			
				webApp.app.vars["runtime"].push({
					"source" : msg,
					"ms" : ms,
					"sec" : ms / 1000
				});
				
				if( typeof p["callback2"] === "function"){
					p["callback2"]();//return from _buildBlock()
				}
				
			},//end callback
			"callback2" : null
		};
		//extend p object
		for(var key in opt ){
			p[key] = opt[key];
		}
// console.log(p);
	
		// if( p["content"].length === 0 ){
// _log("<p>app.buildBlock,   error, content is <b class='text-danger'>empty</b></p>");
			// return false;
		// }

		//dynamic form content
		if( typeof p["content"] === "function"){
			p["content"]({
				"callback" : function( res ){
//console.log(res);								
					var html = webApp.draw.wrapContent({
						"data" : res,
						//"type" : "menu",//"list"
						//"contentType" : p["contentType"],
						"templateID" : p["contentTpl"],
						"templateListID" : p["contentListTpl"]
					});
					
//console.log(html);								
					//var html = "<h1>Test!!!</h1>";
					if( html && html.length > 0){
						p["content"] = html;
						webApp.draw.insertBlock( p );
					}
					
				}
			});
		} else {
			webApp.draw.insertBlock( p );
		}

	};//end _buildBlock()

*/

//============================== TEMPLATES

	function _loadTemplates( callback ){
		//webApp.db.loadTemplates(function( isLoadTemplates ){
//console.log(isLoadTemplates);			
			//if( !isLoadTemplates ){
				_loadTemplatesFromFile();
			//} else{
				//if( typeof callback === "function"){
					//callback();
				//}
			//}
		//});//end db.loadTemplates()
		
		function _loadTemplatesFromFile(){
			
			if( !webApp.vars["templates_url"] || 
				webApp.vars["templates_url"].length === 0 ){
webApp.vars["logMsg"] = "- error, _loadTemplates(), not found 'templates_url'...";
func.logAlert( webApp.vars["logMsg"], "danger");
//console.log( webApp.vars["logMsg"] );
				if( typeof callback === "function"){
					callback(false);
				}
				return false;
			}
			
			func.runAjax({
				"requestMethod" : "GET", 
				"url" : webApp.vars["templates_url"], 
				//"onProgress" : function( e ){},
				//"onLoadEnd" : function( headers ){},
				"onError" : function( xhr ){
//console.log( "onError ", arguments);
webApp.vars["logMsg"] = "error ajax load " + webApp.vars["templates_url"];
func.logAlert( webApp.vars["logMsg"], "danger");
console.log( webApp.vars["logMsg"] );
					if( typeof callback === "function"){
						callback(false);
					}
					return false;
				},
				
				"callback": function( data ){
webApp.vars["logMsg"] = "- read templates from <b>" + webApp.vars["templates_url"] +"</b>";
func.logAlert( webApp.vars["logMsg"], "info");
//console.log( webApp.vars["logMsg"] );
//console.log( data );

					if( !data ){
console.log("error, loadTemplates(), not find data templates'....");
						if( typeof callback === "function"){
							callback(false);
						}
						return false;
					}

					try{
						//xmlNodes = func.convertXmlToObj( data );
						xmlNodes = func.parseXmlToObj( func, data );
//console.log(xmlNodes);
						if( xmlNodes.length > 0 ){
							for( var n= 0; n < xmlNodes.length; n++){
								var key = xmlNodes[n]["name"];

								var value = xmlNodes[n]["html_code"]
								.replace(/<!--([\s\S]*?)-->/mig,"")//remove comments
								.replace(/\t/g,"")
								.replace(/\n/g,"");
								
								webApp.vars["templates"][key] = value;
							}//next
							delete xmlNodes;
							
							//webApp.db.saveTemplates( webApp.draw.vars["templates"] );
						} else {
	console.log("error, loadTemplates(), cannot parse templates data.....");
						}
						
					} catch(e){
console.log(e, typeof e);
webApp.vars["logMsg"] = "TypeError: " + e;
func.logAlert( webApp.vars["logMsg"], "danger");
					}//end try

					if( typeof callback === "function"){
						callback();
					}
				}//end
			});
			
		}//end _loadTemplatesFromFile()
		
	}//end _loadTemplates()
	
	
	
	
	// public interfaces
	return{
		vars : _vars,
		init:	function(){
			return _init();
		}
	};
	
};//end _app()


//============================ test modal
/*
	var overlay = func.getById("overlay");
	if( overlay ){
		//overlay.className="modal-backdrop in";
		overlay.style.display="block";
	}
	var waitWindow = func.getById("wait-window");
	if( waitWindow ){
		//waitWindow.className="modal-dialog";
		waitWindow.style.display="block";
	}
*/

	//var waitWindow = func.getById("win1");
	//if( waitWindow ){
		//waitWindow.style.display="block";
	//}
/*
setTimeout(function(){

		//hide block overlay and wait window
		//if( overlay ){
			//overlay.className="";
			//overlay.style.display="none";
		//}
		if( waitWindow ){
			waitWindow.style.display="none";
		}
}, 1000*3);
*/


//=================================== CACHE, STORAGES
/*
			if ( config["use_localcache"] ) {
				
				//load localforage script
				var script = document.createElement('script');
				//script.src = "js/vendor/localforage.min.js";
				script.src = config["localforagePath"];
				document.getElementsByTagName('head')[0].appendChild(script);
				
				script.onload = function() {
//console.log( "onload " + this.src);
var logMsg = "<p class='alert alert-success'>onload " + this.src +"</p>";
//func.log(logMsg);
_vars["info"].push(logMsg);
					_postLoadStorageScript();
				};
				
				script.onerror = function(e) {
					alert( "error load script " + this.src);
				}; 
				
			} else {

				load_xml({
					filename : config["xml_file"],
					//dataType: "text",
					dataType: "xml",
					callback: function(data){
//console.log(typeof data, data[0]);
						
						_parseXML({
							"xml":data
						});
						_loadApp();
					}
				});
			}
			
			
			function _postLoadStorageScript(){
				
				var res = storage.init();
//for TEST!!!
//res = false;
				if( res ){//cache is available
//----------- hide not used progress bar
//$(_vars["loadProgressBar"]).parent().parent().hide();
//$("#load-progress").hide();
//-----------
					storage.checkAppData({
						"callback": function(){
//console.log( "storage.checkAppData(), end process");
//for TEST!!!
//storage["need_update"] = false;
							if(storage["need_update"]){
								_updateStorage();
							} 
								
							if(!storage["need_update"]){
//for TEST!!!
//storage.getXml();
								//storage.getAppData({
									//"callback": function(){
	//console.log( "storage.getAppData(), end process");
										_loadApp();
									//}
								//});
								
							}
								
						}//end callback
					});//end storage.checkAppData()
				}
					
				if( !res ){//cache is unavailable
					config["use_localcache"] = false;
					load_xml({
						filename : config["xml_file"],
						dataType: "text",
						//dataType: "xml",
						callback: function(data){
//console.log(typeof data, data[0]);

							if(!data){
var logMsg = "<p class='alert alert-danger'>Book catalog not loaded.</p>";
func.log(logMsg);
								_hideWaitWindow()
								return false;
							}
							
							_parseXML({
								"xml":data
							});
							_loadApp();
							
						}
					});
				}
					
			}//end _postLoadStorageScript()

			function _updateStorage(){
				load_xml({
					filename : config["xml_file"],
					dataType: "xml",
					callback: function(data){
//console.log(typeof data, data);							
				
						if(!data){
				var logMsg = "<p class='alert alert-danger'>Book catalog not loaded.</p>";
func.log(logMsg);
							_hideWaitWindow()
							return false;
						}

						_parseXML({
							"xml":data
						});
							
						storage.saveAppData({
							"callback": function(){
console.log( "storage.saveAppData(), end process");
								_loadApp();
							}
						});
							
					}//end load_xml() callback
				});
			}//end __updateStorage()	

*/

//======================== breadcrumb
		function _formBreadcrumb( target ){
			
			if( $(target).hasClass("root-link") ){
				_vars["breadcrumb"] = {};
			}
			
			//form unique key 
			var breadcrumbKey = _vars["GET"]["q"];
			for( var key in _vars["GET"]){
				if( key === "q"){
					continue;
				}
				breadcrumbKey += _vars["GET"][key];
			}
			//console.log(breadcrumbKey);

			_vars["breadcrumb"][ breadcrumbKey ] = {
				"title" : $(target).text(),
				"url" : $(target).attr("href")
			};
			_vars["currentUrl"] = $(target).attr("href");
			
			draw.renderBreadCrumb({
				"breadcrumb": _vars["breadcrumb"],
				"template": _vars["templates"]["breadcrumb_item_tpl"],
				"currentUrl": _vars["currentUrl"]
			});
			
		}//end _formBreadCrumb()
